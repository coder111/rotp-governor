// 
// Decompiled by Procyon v0.5.36
// 

package rotp.ui.planets;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import rotp.ui.ExitButton;
import java.util.Iterator;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferedImage;
import rotp.model.ships.Design;
import rotp.model.colony.Colony;
import rotp.ui.main.MainUI;
import java.awt.Polygon;
import java.awt.event.MouseWheelListener;
import java.awt.image.ImageObserver;
import java.awt.Image;
import java.awt.geom.Ellipse2D;
import java.awt.event.ActionListener;
import rotp.ui.main.EmpireColonyInfoPane;
import rotp.ui.main.EmpireColonyFoundedPane;
import javax.swing.border.Border;
import rotp.util.ShadowBorder;
import java.awt.event.MouseEvent;
import java.awt.Stroke;
import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;
import rotp.ui.main.SystemPanel;
import java.awt.Dimension;
import java.awt.geom.Area;
import java.awt.Rectangle;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JPanel;
import rotp.model.empires.Empire;
import rotp.ui.RotPUI;
import java.awt.Paint;
import java.awt.geom.Point2D;
import java.awt.Graphics2D;
import java.awt.Graphics;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.Action;
import javax.swing.KeyStroke;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;
import java.awt.Insets;
import javax.swing.BorderFactory;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.BorderLayout;
import java.awt.LinearGradientPaint;
import rotp.ui.main.EmpireColonySpendingPane;
import rotp.ui.fleets.SystemListingUI;
import java.util.HashMap;
import rotp.model.galaxy.StarSystem;
import java.util.List;
import rotp.ui.BaseTextField;
import rotp.util.Palette;
import java.awt.Color;
import rotp.ui.SystemViewer;
import rotp.ui.BasePanel;

public class PlanetsUI extends BasePanel implements SystemViewer
{
    private static final long serialVersionUID = -2978024189696705022L;
    private static final int ECOLOGY_MODE = 1;
    private static final int INDUSTRY_MODE = 2;
    private static final int MILITARY_MODE = 3;
    private static int selectedMode;
    private static final Color selectedC;
    private static final Color unselectedC;
    private static final Color darkBrown;
    private static final Color brown;
    private static final Color sliderBoxBlue;
    private static Palette palette;
    private static BaseTextField notesField;
    private static BaseTextField nameField;
    static final int UP_ACTION = 1;
    static final int DOWN_ACTION = 2;
    static final String CANCEL_ACTION = "cancel-input";
    private int pad;
    private List<StarSystem> displayedSystems;
    private final HashMap<Integer, SystemListingUI.DataView> views;
    private final TransferReserveUI transferReservePane;
    private final PlanetDisplayPanel planetDisplayPane;
    private final PlanetViewSelectionPanel viewSelectionPane;
    private EmpireColonySpendingPane spendingPane;
    private final PlanetsUI instance;
    private final PlanetListingUI planetListing;
    private PlanetDataListingUI listingUI;
    private LinearGradientPaint backGradient;
    
    public PlanetsUI() {
        this.pad = 10;
        this.views = new HashMap<Integer, SystemListingUI.DataView>();
        this.viewSelectionPane = new PlanetViewSelectionPanel();
        PlanetsUI.palette = Palette.named("Brown");
        this.pad = PlanetsUI.s10;
        (this.instance = this).initTextFields();
        this.transferReservePane = new TransferReserveUI();
        this.planetDisplayPane = new PlanetDisplayPanel(this);
        this.planetListing = new PlanetListingUI(this);
        this.init();
    }
    
    private void init() {
        final BasePanel centerPanel = new BasePanel();
        centerPanel.setOpaque(false);
        centerPanel.setLayout(new BorderLayout());
        centerPanel.add(this.planetListing, "Center");
        centerPanel.add(new EmpireRevenueUI(), "South");
        final BasePanel rightPanel = new BasePanel();
        rightPanel.setOpaque(false);
        rightPanel.setLayout(new BorderLayout(0, PlanetsUI.s22));
        rightPanel.add(this.planetDisplayPane, "Center");
        rightPanel.add(new ExitPlanetsButton(this.getWidth(), PlanetsUI.s60, PlanetsUI.s10, PlanetsUI.s2), "South");
        this.setBackground(Color.black);
        this.setBorder(BorderFactory.createEmptyBorder(PlanetsUI.s10, PlanetsUI.s10, PlanetsUI.s10, PlanetsUI.s10));
        final BorderLayout layout = new BorderLayout();
        layout.setVgap(this.pad);
        layout.setHgap(this.pad);
        this.setLayout(layout);
        this.add(centerPanel, "Center");
        this.add(rightPanel, "East");
        this.initDataViews();
    }
    
    private void initTextFields() {
        (PlanetsUI.notesField = new BaseTextField(this)).setLimit(50);
        (PlanetsUI.nameField = new BaseTextField(this)).setLimit(24);
        PlanetsUI.nameField.setBackground(PlanetsUI.selectedC);
        PlanetsUI.nameField.setBorder(this.newEmptyBorder(10, 5, 0, 0));
        PlanetsUI.nameField.setMargin(new Insets(0, PlanetsUI.s5, 0, 0));
        PlanetsUI.nameField.setFont(this.narrowFont(20));
        PlanetsUI.nameField.setForeground(PlanetsUI.palette.black);
        PlanetsUI.nameField.setCaretColor(PlanetsUI.palette.black);
        PlanetsUI.nameField.putClientProperty("caretWidth", PlanetsUI.s3);
        PlanetsUI.nameField.setFocusTraversalKeysEnabled(false);
        PlanetsUI.nameField.setVisible(false);
        PlanetsUI.nameField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(final KeyEvent e) {
                PlanetsUI.this.setFieldValues(PlanetsUI.this.selectedSystem());
            }
        });
        final InputMap im0 = PlanetsUI.nameField.getInputMap(2);
        final ActionMap am0 = PlanetsUI.nameField.getActionMap();
        im0.put(KeyStroke.getKeyStroke("ESCAPE"), "cancel-input");
        im0.put(KeyStroke.getKeyStroke("UP"), 1);
        im0.put(KeyStroke.getKeyStroke("DOWN"), 2);
        im0.put(KeyStroke.getKeyStroke("TAB"), 2);
        im0.put(KeyStroke.getKeyStroke("ENTER"), 2);
        am0.put(1, new UpAction());
        am0.put(2, new DownAction());
        am0.put("cancel-input", new CancelAction());
        PlanetsUI.notesField.setBackground(PlanetsUI.selectedC);
        PlanetsUI.notesField.setBorder(this.newEmptyBorder(10, 5, 0, 0));
        PlanetsUI.notesField.setMargin(new Insets(0, PlanetsUI.s5, 0, 0));
        PlanetsUI.notesField.setFont(this.narrowFont(20));
        PlanetsUI.notesField.setForeground(PlanetsUI.palette.black);
        PlanetsUI.notesField.setCaretColor(PlanetsUI.palette.black);
        PlanetsUI.notesField.putClientProperty("caretWidth", PlanetsUI.s3);
        PlanetsUI.notesField.setFocusTraversalKeysEnabled(false);
        PlanetsUI.notesField.setVisible(false);
        PlanetsUI.notesField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(final KeyEvent e) {
                PlanetsUI.this.setFieldValues(PlanetsUI.this.selectedSystem());
            }
        });
        final InputMap im2 = PlanetsUI.notesField.getInputMap(2);
        final ActionMap am2 = PlanetsUI.notesField.getActionMap();
        im2.put(KeyStroke.getKeyStroke("ESCAPE"), "cancel-input");
        im2.put(KeyStroke.getKeyStroke("UP"), 1);
        im2.put(KeyStroke.getKeyStroke("DOWN"), 2);
        im2.put(KeyStroke.getKeyStroke("TAB"), 2);
        im2.put(KeyStroke.getKeyStroke("ENTER"), 2);
        am2.put(1, new UpAction());
        am2.put(2, new DownAction());
        am2.put("cancel-input", new CancelAction());
    }
    
    @Override
    public String subPanelTextureName() {
        return "TEXTURE_BROWN";
    }
    
    @Override
    public boolean drawMemory() {
        return true;
    }
    
    @Override
    public boolean hasStarBackground() {
        return true;
    }
    
    @Override
    public void animate() {
        if (!this.playAnimations()) {
            return;
        }
        this.planetListing.animate();
    }
    
    @Override
    public void paintComponent(final Graphics g0) {
        super.paintComponent(g0);
        final Graphics2D g = (Graphics2D)g0;
        if (this.backGradient == null) {
            final Color c0 = new Color(71, 53, 39, 0);
            final Color c2 = new Color(71, 53, 39);
            final Point2D start = new Point2D.Float((float)PlanetsUI.s10, (float)(this.getHeight() - this.scaled(200)));
            final Point2D end = new Point2D.Float((float)PlanetsUI.s10, (float)(this.getHeight() - PlanetsUI.s20));
            final float[] dist = { 0.0f, 1.0f };
            final Color[] colors = { c0, c2 };
            this.backGradient = new LinearGradientPaint(start, end, dist, colors);
        }
        g.setPaint(this.backGradient);
        g.fillRect(PlanetsUI.s10, this.getHeight() - this.scaled(200), this.getWidth() - PlanetsUI.s20, this.scaled(190));
    }
    
    @Override
    public void keyPressed(final KeyEvent e) {
        boolean repaint = false;
        final int mods = e.getModifiersEx();
        switch (e.getKeyCode()) {
            case 27: {
                if (this.frame().getGlassPane().isVisible()) {
                    this.disableGlassPane();
                }
                else {
                    this.finish(false);
                }
                return;
            }
            case 9: {
                if (mods == 0) {
                    this.viewSelectionPane.selectNextTab();
                }
                else if (mods == 1) {
                    this.viewSelectionPane.selectPreviousTab();
                }
                return;
            }
            case 38: {
                repaint = this.listingUI.scrollUp();
                break;
            }
            case 40: {
                repaint = this.listingUI.scrollDown();
                break;
            }
            case KeyEvent.VK_Q:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53: {
                this.spendingPane.keyPressed(e);
                return;
            }
        }
        if (repaint) {
            this.repaint();
        }
    }
    
    private List<StarSystem> systems() {
        if (this.displayedSystems == null) {
            this.displayedSystems = this.player().allColonizedSystems();
            if (this.selectedSystem() == null) {
                this.selectedSystem(this.displayedSystems.get(0), false);
            }
            this.selectedSystem(this.selectedSystem(), false);
        }
        return this.displayedSystems;
    }
    
    void systems(final List<StarSystem> s) {
        this.displayedSystems = s;
    }
    
    @Override
    public StarSystem systemViewToDisplay() {
        return this.selectedSystem();
    }
    
    private synchronized void selectedSystem(final StarSystem sys, final boolean updateFieldValues) {
        PlanetsUI.notesField.setVisible(false);
        PlanetsUI.nameField.setVisible(false);
        RotPUI.instance().requestFocusInWindow();
        if (updateFieldValues) {
            this.setFieldValues(this.selectedSystem());
        }
        this.sessionVar("MAINUI_SELECTED_SYSTEM", sys);
        this.sessionVar("MAINUI_CLICKED_SPRITE", sys);
        PlanetsUI.notesField.setText(sys.notes());
        PlanetsUI.notesField.setCaretPosition(PlanetsUI.notesField.getText().length());
        PlanetsUI.nameField.setText(this.player().sv.name(sys.id));
        PlanetsUI.nameField.setCaretPosition(PlanetsUI.nameField.getText().length());
    }
    
    private void setFieldValues(final StarSystem sys) {
        if (sys != null) {
            final Empire pl = this.player();
            sys.notes(PlanetsUI.notesField.getText().trim());
            final String name = PlanetsUI.nameField.getText().trim();
            if (!name.isEmpty()) {
                pl.sv.name(sys.id, name);
            }
        }
    }
    
    private StarSystem selectedSystem() {
        StarSystem sys = (StarSystem)this.sessionVar("MAINUI_SELECTED_SYSTEM");
        final Empire pl = this.player();
        final int id = sys.id;
        if (pl.sv.empire(id) != pl) {
            sys = pl.defaultSystem();
        }
        return (sys == null || !this.systems().contains(sys)) ? null : sys;
    }
    
    private void initDataViews() {
        final SystemListingUI.Column rowNumCol = this.listingUI.newRowNumColumn("PLANETS_LIST_NUM", 15, 1);
        final SystemListingUI.Column nameCol = this.listingUI.newSystemNameColumn(PlanetsUI.nameField, "PLANETS_LIST_NAME", "NAME", 170, PlanetsUI.palette.black, StarSystem.NAME, -1);
        final SystemListingUI.Column populationCol = this.listingUI.newSystemDeltaDataColumn("PLANETS_LIST_POPULATION", "POPULATION", 130, PlanetsUI.palette.black, StarSystem.POPULATION, 1);
        final SystemListingUI.Column sizeCol = this.listingUI.newSystemDataColumn("PLANETS_LIST_SIZE", "SIZE", 60, PlanetsUI.palette.black, StarSystem.CURRENT_SIZE, 1);
        final SystemListingUI.Column pTypeCol = this.listingUI.newPlanetTypeColumn("PLANETS_LIST_TYPE", "PLANET_TYPE", 90, StarSystem.PLANET_TYPE);
        final SystemListingUI.Column wasteCol = this.listingUI.newSystemDataColumn("PLANETS_LIST_WASTE", "WASTE", 75, PlanetsUI.palette.black, StarSystem.WASTE, 1);
        final SystemListingUI.Column notesCol = this.listingUI.newSystemNotesColumn(PlanetsUI.notesField, "PLANETS_LIST_NOTES", "NOTES", 999, PlanetsUI.palette.black);
        final SystemListingUI.Column factoriesCol = this.listingUI.newSystemDeltaDataColumn("PLANETS_LIST_FACTORIES", "FACTORIES", 110, PlanetsUI.palette.black, StarSystem.FACTORIES, 1);
        final SystemListingUI.Column productionCol = this.listingUI.newSystemDataColumn("PLANETS_LIST_PRODUCTION", "INCOME", 60, PlanetsUI.palette.black, StarSystem.INCOME, 1);
        final SystemListingUI.Column indRsvCol = this.listingUI.newSystemDataColumn("PLANETS_LIST_RESERVE", "RESERVE", 60, PlanetsUI.palette.black, StarSystem.INDUSTRY_RESERVE, 1);
        final SystemListingUI.Column basesCol = this.listingUI.newSystemDeltaDataColumn("PLANETS_LIST_BASES", "BASES", 70, PlanetsUI.palette.black, StarSystem.BASES, 1);
        final SystemListingUI.Column shieldCol = this.listingUI.newSystemDataColumn("PLANETS_LIST_SHIELD", "SHIELD", 70, PlanetsUI.palette.black, StarSystem.SHIELD, 1);
        final SystemListingUI.Column shipCol = this.listingUI.newSystemDataColumn("PLANETS_LIST_SHIPYARD", "SHIPYARD", 120, PlanetsUI.palette.black, StarSystem.SHIPYARD, -1);
        final SystemListingUI.Column resourceCol = this.listingUI.newSystemDataColumn("PLANETS_LIST_RESOURCES", "RESOURCES", 90, PlanetsUI.palette.black, StarSystem.RESOURCES, -1);
        final SystemListingUI.DataView ecoView = this.listingUI.newDataView();
        ecoView.addColumn(rowNumCol);
        ecoView.addColumn(nameCol);
        ecoView.addColumn(populationCol);
        ecoView.addColumn(pTypeCol);
        ecoView.addColumn(resourceCol);
        ecoView.addColumn(sizeCol);
        ecoView.addColumn(wasteCol);
        ecoView.addColumn(notesCol);
        this.views.put(1, ecoView);
        final SystemListingUI.DataView indView = this.listingUI.newDataView();
        indView.addColumn(rowNumCol);
        indView.addColumn(nameCol);
        indView.addColumn(populationCol);
        indView.addColumn(resourceCol);
        indView.addColumn(factoriesCol);
        indView.addColumn(productionCol);
        indView.addColumn(indRsvCol);
        indView.addColumn(notesCol);
        this.views.put(2, indView);
        final SystemListingUI.DataView milView = this.listingUI.newDataView();
        milView.addColumn(rowNumCol);
        milView.addColumn(nameCol);
        milView.addColumn(populationCol);
        milView.addColumn(resourceCol);
        milView.addColumn(productionCol);
        milView.addColumn(shieldCol);
        milView.addColumn(basesCol);
        milView.addColumn(shipCol);
        milView.addColumn(notesCol);
        this.views.put(3, milView);
        this.listingUI.selectedColumn(rowNumCol);
    }
    
    private void finish(final boolean disableNextTurn) {
        this.displayedSystems = null;
        this.hardClick();
        RotPUI.instance().selectMainPanel(disableNextTurn);
    }
    
    static {
        PlanetsUI.selectedMode = 1;
        selectedC = new Color(178, 124, 87);
        unselectedC = new Color(112, 85, 68);
        darkBrown = new Color(45, 14, 5);
        brown = new Color(64, 24, 13);
        sliderBoxBlue = new Color(34, 140, 142);
    }
    
    class PlanetListingUI extends BasePanel
    {
        private static final long serialVersionUID = -2022835351135736762L;
        
        public PlanetListingUI(final PlanetsUI p) {
            this.init(p);
        }
        
        private void init(final PlanetsUI p) {
            this.setOpaque(false);
            this.setLayout(new BorderLayout());
            PlanetsUI.this.listingUI = new PlanetDataListingUI(p);
            final JPanel centerPanel = new JPanel();
            centerPanel.setOpaque(false);
            centerPanel.setLayout(new BorderLayout());
            centerPanel.add(PlanetsUI.this.viewSelectionPane, "North");
            centerPanel.add(PlanetsUI.this.listingUI, "Center");
            this.add(centerPanel, "Center");
        }
        
        @Override
        public void animate() {
            PlanetsUI.this.planetDisplayPane.animate();
        }
    }
    
    class PlanetViewSelectionPanel extends BasePanel implements MouseMotionListener, MouseListener
    {
        private static final long serialVersionUID = 5653424859339696558L;
        Rectangle hoverBox;
        Rectangle ecologyBox;
        Rectangle industryBox;
        Rectangle militaryBox;
        Area textureArea;
        
        public PlanetViewSelectionPanel() {
            this.ecologyBox = new Rectangle();
            this.industryBox = new Rectangle();
            this.militaryBox = new Rectangle();
            this.initModel();
        }
        
        private void initModel() {
            this.setOpaque(false);
            this.setPreferredSize(new Dimension(this.getWidth(), PlanetViewSelectionPanel.s40));
            this.addMouseListener(this);
            this.addMouseMotionListener(this);
        }
        
        @Override
        public Area textureArea() {
            return this.textureArea;
        }
        
        @Override
        public String textureName() {
            return PlanetsUI.this.instance.subPanelTextureName();
        }
        
        @Override
        public void paintComponent(final Graphics g0) {
            super.paintComponent(g0);
            final Graphics2D g = (Graphics2D)g0;
            final int w = this.getWidth();
            final int h = this.getHeight();
            final int gap = PlanetViewSelectionPanel.s20;
            final int tabW = (w - 6 * gap) / 4;
            final String title = this.text("PLANETS_TITLE", this.player().raceName());
            final String ecoLabel = this.text("PLANETS_VIEW_ECOLOGY");
            final String indLabel = this.text("PLANETS_VIEW_INDUSTRY");
            final String milLabel = this.text("PLANETS_VIEW_MILITARY");
            g.setColor(SystemPanel.orangeText);
            g.setFont(this.narrowFont(30));
            int x0 = gap;
            final int y0 = h - PlanetViewSelectionPanel.s10;
            g.drawString(title, x0, y0);
            x0 += tabW + gap;
            this.drawTab(g, x0, 0, tabW, h, ecoLabel, this.ecologyBox, PlanetsUI.selectedMode == 1);
            this.textureArea = new Area(new RoundRectangle2D.Float((float)x0, (float)PlanetViewSelectionPanel.s10, (float)tabW, (float)(h - PlanetViewSelectionPanel.s10), (float)(h / 4), (float)(h / 4)));
            x0 += tabW + gap;
            this.drawTab(g, x0, 0, tabW, h, indLabel, this.industryBox, PlanetsUI.selectedMode == 2);
            final Area tab2Area = new Area(new RoundRectangle2D.Float((float)x0, (float)PlanetViewSelectionPanel.s10, (float)tabW, (float)(h - PlanetViewSelectionPanel.s10), (float)(h / 4), (float)(h / 4)));
            this.textureArea.add(tab2Area);
            x0 += tabW + gap;
            this.drawTab(g, x0, 0, tabW, h, milLabel, this.militaryBox, PlanetsUI.selectedMode == 3);
            final Area tab3Area = new Area(new RoundRectangle2D.Float((float)x0, (float)PlanetViewSelectionPanel.s10, (float)tabW, (float)(h - PlanetViewSelectionPanel.s10), (float)(h / 4), (float)(h / 4)));
            this.textureArea.add(tab3Area);
        }
        
        private void drawTab(final Graphics2D g, final int x, final int y, final int w, final int h, final String label, final Rectangle box, final boolean selected) {
            g.setFont(this.narrowFont(20));
            if (selected) {
                g.setColor(PlanetsUI.selectedC);
            }
            else {
                g.setColor(PlanetsUI.unselectedC);
            }
            box.setBounds(x, y + PlanetViewSelectionPanel.s10, w, h - PlanetViewSelectionPanel.s10);
            g.fillRoundRect(x, y + PlanetViewSelectionPanel.s10, w, h - PlanetViewSelectionPanel.s10, h / 4, h / 4);
            g.fillRect(x, h - PlanetViewSelectionPanel.s5, w, PlanetViewSelectionPanel.s5);
            if (box == this.hoverBox) {
                final Stroke prev = g.getStroke();
                g.setStroke(PlanetViewSelectionPanel.stroke2);
                g.setColor(Color.yellow);
                g.setClip(x, y, w, h * 2 / 3);
                g.drawRoundRect(x, y + PlanetViewSelectionPanel.s10, w, h - PlanetViewSelectionPanel.s10, h / 4, h / 4);
                g.setClip(x, y + h / 2, w, h / 2);
                g.drawRect(x, y + PlanetViewSelectionPanel.s10, w, h);
                g.setClip(null);
                g.setStroke(prev);
            }
            final int sw = g.getFontMetrics().stringWidth(label);
            final int x2 = x + (w - sw) / 2;
            final int y2 = y + h - PlanetViewSelectionPanel.s10;
            final Color c0 = (box == this.hoverBox) ? Color.yellow : SystemPanel.whiteLabelText;
            this.drawShadowedString(g, label, 3, x2, y2, SystemPanel.textShadowC, c0);
        }
        
        public void selectNextTab() {
            switch (PlanetsUI.selectedMode) {
                case 1: {
                    this.selectTab(2);
                    break;
                }
                case 2: {
                    this.selectTab(3);
                    break;
                }
                case 3: {
                    this.selectTab(1);
                    break;
                }
            }
        }
        
        public void selectPreviousTab() {
            switch (PlanetsUI.selectedMode) {
                case 1: {
                    this.selectTab(3);
                    break;
                }
                case 2: {
                    this.selectTab(1);
                    break;
                }
                case 3: {
                    this.selectTab(2);
                    break;
                }
            }
        }
        
        private void selectTab(final int mode) {
            if (mode != PlanetsUI.selectedMode) {
                this.softClick();
                PlanetsUI.selectedMode = mode;
                PlanetsUI.this.selectedSystem(PlanetsUI.this.selectedSystem(), true);
                PlanetsUI.this.instance.repaint();
            }
        }
        
        @Override
        public void mouseClicked(final MouseEvent e) {
        }
        
        @Override
        public void mouseEntered(final MouseEvent e) {
        }
        
        @Override
        public void mouseExited(final MouseEvent e) {
            if (this.hoverBox != null) {
                this.hoverBox = null;
                this.repaint();
            }
        }
        
        @Override
        public void mousePressed(final MouseEvent e) {
        }
        
        @Override
        public void mouseReleased(final MouseEvent e) {
            if (e.getButton() > 3) {
                return;
            }
            final int x = e.getX();
            final int y = e.getY();
            if (this.hoverBox == null) {
                this.misClick();
            }
            else if (this.hoverBox == this.ecologyBox) {
                this.selectTab(1);
            }
            else if (this.hoverBox == this.industryBox) {
                this.selectTab(2);
            }
            else if (this.hoverBox == this.militaryBox) {
                this.selectTab(3);
            }
        }
        
        @Override
        public void mouseDragged(final MouseEvent e) {
        }
        
        @Override
        public void mouseMoved(final MouseEvent e) {
            final int x = e.getX();
            final int y = e.getY();
            final Rectangle prevHover = this.hoverBox;
            if (this.ecologyBox.contains(x, y)) {
                this.hoverBox = this.ecologyBox;
            }
            else if (this.industryBox.contains(x, y)) {
                this.hoverBox = this.industryBox;
            }
            else if (this.militaryBox.contains(x, y)) {
                this.hoverBox = this.militaryBox;
            }
            if (this.hoverBox != prevHover) {
                this.repaint();
            }
        }
    }
    
    class PlanetDisplayPanel extends SystemPanel
    {
        private static final long serialVersionUID = -8822918649736736306L;
        EmpireInfoGraphicPane graphicPane;
        PlanetsUI parent;
        
        public PlanetDisplayPanel(final PlanetsUI p) {
            this.parent = p;
            this.init();
        }
        
        private void init() {
            this.setOpaque(true);
            this.setBackground(PlanetsUI.selectedC);
            this.setBorder(this.newEmptyBorder(6, 6, 6, 6));
            this.setPreferredSize(new Dimension(this.scaled(250), this.getHeight()));
            (this.graphicPane = new EmpireInfoGraphicPane(this)).setPreferredSize(new Dimension(this.getWidth(), this.scaled(140)));
            final BorderLayout layout = new BorderLayout();
            layout.setVgap(PlanetDisplayPanel.s6);
            this.setLayout(layout);
            this.add(this.topPane(), "North");
            this.add(this.detailPane(), "Center");
        }
        
        @Override
        public String subPanelTextureName() {
            return "TEXTURE_BROWN";
        }
        
        @Override
        public StarSystem systemViewToDisplay() {
            return PlanetsUI.this.selectedSystem();
        }
        
        @Override
        public void animate() {
            this.graphicPane.animate();
        }
        
        @Override
        protected BasePanel topPane() {
            return this.graphicPane;
        }
        
        @Override
        protected BasePanel detailPane() {
            final BasePanel empireDetailTopPane = new BasePanel();
            empireDetailTopPane.setOpaque(false);
            empireDetailTopPane.setBorder(new ShadowBorder(PlanetsUI.palette.bdrHiOut, PlanetsUI.palette.bdrLoIn));
            empireDetailTopPane.setLayout(new BorderLayout(0, PlanetDisplayPanel.s1));
            empireDetailTopPane.setPreferredSize(new Dimension(this.getWidth(), this.scaled(120)));
            empireDetailTopPane.add(new EmpireColonyFoundedPane(this, PlanetsUI.unselectedC), "North");
            empireDetailTopPane.add(new EmpireColonyInfoPane(this, PlanetsUI.unselectedC, PlanetsUI.palette.bdrHiIn, PlanetsUI.palette.yellow, PlanetsUI.palette.bdrLoIn), "Center");
            final BasePanel empireDetailBottomPane = new BasePanel();
            empireDetailBottomPane.setOpaque(false);
            empireDetailBottomPane.setBorder(new ShadowBorder(PlanetsUI.palette.bdrHiOut, PlanetsUI.palette.bdrLoIn));
            empireDetailBottomPane.setLayout(new BorderLayout(0, PlanetDisplayPanel.s3));
            empireDetailBottomPane.setPreferredSize(new Dimension(this.getWidth(), this.scaled(215)));
            empireDetailBottomPane.add(new ColonyShipPane(this), "North");
            empireDetailBottomPane.add(new ColonyTransferFunds(this), "Center");
            empireDetailBottomPane.setBorder(this.newEmptyBorder(0, 0, 0, 0));
            PlanetsUI.this.spendingPane = new EmpireColonySpendingPane(this.parent, PlanetsUI.unselectedC, PlanetsUI.palette.white, PlanetsUI.palette.bdrHiOut, PlanetsUI.palette.bdrLoIn);
            final BasePanel empireDetailPane = new BasePanel();
            empireDetailPane.setOpaque(false);
            empireDetailPane.setLayout(new BorderLayout(0, PlanetDisplayPanel.s3));
            empireDetailPane.add(empireDetailTopPane, "North");
            empireDetailPane.add(PlanetsUI.this.spendingPane, "Center");
            empireDetailPane.add(empireDetailBottomPane, "South");
            return empireDetailPane;
        }
    }
    
    class EmpireInfoGraphicPane extends BasePanel implements ActionListener
    {
        private static final long serialVersionUID = 5989755294241869472L;
        SystemPanel parent;
        Ellipse2D starCircle;
        Ellipse2D planetCircle;
        int currentHover;
        
        EmpireInfoGraphicPane(final SystemPanel p) {
            this.starCircle = new Ellipse2D.Float();
            this.planetCircle = new Ellipse2D.Float();
            this.currentHover = 0;
            this.parent = p;
            this.init();
        }
        
        private void init() {
            this.setBackground(PlanetsUI.palette.black);
        }
        
        @Override
        public void paintComponent(final Graphics g) {
            super.paintComponent(g);
            final StarSystem sys = this.parent.systemViewToDisplay();
            if (sys == null) {
                return;
            }
            final Empire pl = this.player();
            final int w = this.getWidth();
            final int h = this.getHeight();
            final Graphics2D g2 = (Graphics2D)g;
            g2.drawImage(pl.sv.starBackground(this), 0, 0, null);
            this.drawStar(g2, PlanetsUI.this.selectedSystem().starType(), EmpireInfoGraphicPane.s80, this.getWidth() / 3, EmpireInfoGraphicPane.s70);
            this.starCircle.setFrame(this.getWidth() / 3 - EmpireInfoGraphicPane.s20, EmpireInfoGraphicPane.s10, EmpireInfoGraphicPane.s40, EmpireInfoGraphicPane.s40);
            g.setFont(this.narrowFont(36));
            final String str = this.player().sv.name(sys.id);
            final int y0 = EmpireInfoGraphicPane.s42;
            final int x0 = EmpireInfoGraphicPane.s25;
            this.drawBorderedString(g, str, 2, x0, y0, Color.black, SystemPanel.orangeText);
            final int x2 = EmpireInfoGraphicPane.s20;
            final int y2 = EmpireInfoGraphicPane.s70;
            final int r = EmpireInfoGraphicPane.s40;
            PlanetsUI.this.selectedSystem().planet().draw(g, w, h, x2, y2, r + r, 45);
            this.planetCircle.setFrame(x2, y2, r + r, r + r);
            this.parent.drawPlanetInfo(g2, PlanetsUI.this.selectedSystem(), false, false, EmpireInfoGraphicPane.s40, this.getWidth(), this.getHeight() - EmpireInfoGraphicPane.s12);
        }
        
        @Override
        public void animate() {
            if (this.animationCount() % 3 == 0) {
                try {
                    PlanetsUI.this.selectedSystem().planet().rotate(1.0f);
                    this.repaint();
                }
                catch (Exception ex) {}
            }
        }
    }
    
    class ColonyShipPane extends BasePanel implements MouseListener, MouseMotionListener, MouseWheelListener
    {
        private static final long serialVersionUID = 7752365560363051366L;
        SystemPanel parent;
        private final int[] leftButtonX;
        private final int[] leftButtonY;
        private final int[] rightButtonX;
        private final int[] rightButtonY;
        private final Rectangle shipDesignBox;
        private final Rectangle shipNameBox;
        private final Polygon prevDesign;
        private final Polygon nextDesign;
        private Shape hoverBox;
        private final Color textColor;
        
        ColonyShipPane(final SystemPanel p) {
            this.leftButtonX = new int[3];
            this.leftButtonY = new int[3];
            this.rightButtonX = new int[3];
            this.rightButtonY = new int[3];
            this.shipDesignBox = new Rectangle();
            this.shipNameBox = new Rectangle();
            this.prevDesign = new Polygon();
            this.nextDesign = new Polygon();
            this.textColor = this.newColor(204, 204, 204);
            this.parent = p;
            this.init();
        }
        
        @Override
        public String textureName() {
            return this.parent.subPanelTextureName();
        }
        
        private void init() {
            this.setBackground(PlanetsUI.unselectedC);
            this.setPreferredSize(new Dimension(this.getWidth(), this.scaled(110)));
            this.addMouseListener(this);
            this.addMouseMotionListener(this);
            this.addMouseWheelListener(this);
        }
        
        @Override
        public void paintComponent(final Graphics g0) {
            super.paintComponent(g0);
            final Graphics2D g = (Graphics2D)g0;
            final int w = this.getWidth();
            final int h = this.getHeight();
            final int midMargin = this.scaled(105);
            this.drawTitle(g);
            this.drawShipIcon(g, ColonyShipPane.s5, ColonyShipPane.s30, midMargin - ColonyShipPane.s15, ColonyShipPane.s75);
            this.drawShipCompletion(g, midMargin, h - ColonyShipPane.s75, w - ColonyShipPane.s10 - midMargin, ColonyShipPane.s30);
            this.drawNameSelector(g, midMargin, h - ColonyShipPane.s60, w - ColonyShipPane.s10 - midMargin, ColonyShipPane.s30);
        }
        
        private void drawTitle(final Graphics g) {
            g.setFont(this.narrowFont(20));
            g.setColor(Color.black);
            final String str = this.text("MAIN_COLONY_SHIPYARD_CONSTRUCTION");
            this.drawShadowedString(g, str, 2, ColonyShipPane.s5, ColonyShipPane.s22, MainUI.shadeBorderC(), this.textColor);
        }
        
        private void drawShipIcon(final Graphics2D g, final int x, final int y, final int w, final int h) {
            g.setColor(Color.black);
            g.fillRect(x, y, w, h);
            final StarSystem sys = this.parent.systemViewToDisplay();
            final Colony c = (sys == null) ? null : sys.colony();
            if (c == null) {
                return;
            }
            this.shipDesignBox.setBounds(x, y, w, h);
            final Design d = c.shipyard().design();
            g.drawImage(this.initializedBackgroundImage(w, h), x, y, null);
            final int y0a = h / 3;
            final int y0b = h * 2 / 3;
            g.setColor(PlanetsUI.darkBrown);
            g.fillRect(x, y + ColonyShipPane.s4, w, ColonyShipPane.s4);
            g.fillRect(x, y + h - ColonyShipPane.s8, w, ColonyShipPane.s4);
            g.fillRect(x, y + y0a, w, ColonyShipPane.s4);
            g.fillRect(x, y + y0b - ColonyShipPane.s4, w, ColonyShipPane.s4);
            g.setColor(PlanetsUI.brown);
            g.fillRect(x, y + ColonyShipPane.s8, w, ColonyShipPane.s4);
            g.fillRect(x, y + h - ColonyShipPane.s12, w, ColonyShipPane.s4);
            g.fillRect(x, y + y0a - ColonyShipPane.s4, w, ColonyShipPane.s4);
            g.fillRect(x, y + y0b, w, ColonyShipPane.s4);
            g.setColor(PlanetsUI.darkBrown);
            g.fillRect(x + w / 8, y + y0a + ColonyShipPane.s4, ColonyShipPane.s4, y0b - y0a - ColonyShipPane.s8);
            g.fillRect(x + w * 3 / 8, y + y0a + ColonyShipPane.s4, ColonyShipPane.s4, y0b - y0a - ColonyShipPane.s8);
            g.fillRect(x + w * 5 / 8, y + y0a + ColonyShipPane.s4, ColonyShipPane.s4, y0b - y0a - ColonyShipPane.s8);
            g.fillRect(x + w * 7 / 8, y + y0a + ColonyShipPane.s4, ColonyShipPane.s4, y0b - y0a - ColonyShipPane.s8);
            Stroke prevStroke = g.getStroke();
            g.setStroke(ColonyShipPane.stroke2);
            g.drawLine(x + ColonyShipPane.s4, y + ColonyShipPane.s12, x + w / 5 - ColonyShipPane.s4, y + y0a - ColonyShipPane.s4);
            g.drawLine(x + w / 5 + ColonyShipPane.s4, y + y0a - ColonyShipPane.s4, x + w * 2 / 5 - ColonyShipPane.s4, y + ColonyShipPane.s12);
            g.drawLine(x + w * 2 / 5 + ColonyShipPane.s4, y + ColonyShipPane.s12, x + w * 3 / 5 - ColonyShipPane.s4, y + y0a - ColonyShipPane.s4);
            g.drawLine(x + w * 3 / 5 + ColonyShipPane.s4, y + y0a - ColonyShipPane.s4, x + w * 4 / 5 - ColonyShipPane.s4, y + ColonyShipPane.s12);
            g.drawLine(x + w * 4 / 5 + ColonyShipPane.s4, y + ColonyShipPane.s12, x + w - ColonyShipPane.s4, y + y0a - ColonyShipPane.s4);
            g.drawLine(x + ColonyShipPane.s4, y + h - ColonyShipPane.s12, x + w / 5 - ColonyShipPane.s4, y + y0b + ColonyShipPane.s4);
            g.drawLine(x + w / 5 + ColonyShipPane.s4, y + y0b + ColonyShipPane.s4, x + w * 2 / 5 - ColonyShipPane.s4, y + h - ColonyShipPane.s12);
            g.drawLine(x + w * 2 / 5 + ColonyShipPane.s4, y + h - ColonyShipPane.s12, x + w * 3 / 5 - ColonyShipPane.s4, y + y0b + ColonyShipPane.s4);
            g.drawLine(x + w * 3 / 5 + ColonyShipPane.s4, y + y0b + 4, x + w * 4 / 5 - 4, y + h - 12);
            g.drawLine(x + w * 4 / 5 + ColonyShipPane.s4, y + h - ColonyShipPane.s12, x + w - ColonyShipPane.s4, y + y0b + ColonyShipPane.s4);
            g.setStroke(prevStroke);
            final Image img = d.image();
            final int w2 = img.getWidth(null);
            final int h2 = img.getHeight(null);
            final float scale = this.min(w / (float)w2, h / (float)h2);
            final int w3 = (int)(scale * w2);
            final int h3 = (int)(scale * h2);
            final int x2 = x + (w - w3) / 2;
            final int y2 = y + (h - h3) / 2;
            g.drawImage(img, x2, y2, x2 + w3, y2 + h3, 0, 0, w2, h2, this);
            if (this.hoverBox == this.shipDesignBox) {
                prevStroke = g.getStroke();
                g.setStroke(ColonyShipPane.stroke2);
                g.setColor(SystemPanel.yellowText);
                g.drawRect(x, y, w, h);
                g.setStroke(prevStroke);
            }
            String count;
            if (d.scrapped()) {
                count = this.text("MAIN_COLONY_SHIP_SCRAPPED");
                g.setColor(Color.red);
                g.setFont(this.narrowFont(20));
            }
            else {
                final int i = c.shipyard().upcomingShipCount();
                if (i == 0) {
                    return;
                }
                count = Integer.toString(i);
                g.setColor(SystemPanel.yellowText);
                g.setFont(this.narrowFont(20));
            }
            final int sw = g.getFontMetrics().stringWidth(count);
            g.drawString(count, x + w - ColonyShipPane.s5 - sw, y + h - ColonyShipPane.s5);
        }
        
        private void drawShipCompletion(final Graphics2D g, final int x, final int y, final int w, final int h) {
            final StarSystem sys = this.parent.systemViewToDisplay();
            final Colony c = (sys == null) ? null : sys.colony();
            if (c == null) {
                return;
            }
            final String result = c.shipyard().shipCompletionResult();
            g.setColor(Color.black);
            g.setFont(this.narrowFont(16));
            g.drawString(result, x + ColonyShipPane.s12, y + ColonyShipPane.s10);
        }
        
        private void drawNameSelector(final Graphics2D g, final int x, final int y, final int w, final int h) {
            final StarSystem sys = this.parent.systemViewToDisplay();
            final Colony c = (sys == null) ? null : sys.colony();
            if (c == null) {
                return;
            }
            final int leftM = x;
            final int rightM = x + w;
            final int buttonW = ColonyShipPane.s10;
            final int buttonTopY = y + ColonyShipPane.s5;
            final int buttonMidY = y + ColonyShipPane.s15;
            final int buttonBotY = y + ColonyShipPane.s25;
            this.leftButtonX[0] = leftM;
            this.leftButtonX[1] = leftM + buttonW;
            this.leftButtonX[2] = leftM + buttonW;
            this.leftButtonY[0] = buttonMidY;
            this.leftButtonY[1] = buttonTopY;
            this.leftButtonY[2] = buttonBotY;
            this.rightButtonX[0] = rightM;
            this.rightButtonX[1] = rightM - buttonW;
            this.rightButtonX[2] = rightM - buttonW;
            this.rightButtonY[0] = buttonMidY;
            this.rightButtonY[1] = buttonTopY;
            this.rightButtonY[2] = buttonBotY;
            this.prevDesign.reset();
            this.nextDesign.reset();
            for (int i = 0; i < this.leftButtonX.length; ++i) {
                this.prevDesign.addPoint(this.leftButtonX[i], this.leftButtonY[i]);
                this.nextDesign.addPoint(this.rightButtonX[i], this.rightButtonY[i]);
            }
            if (this.hoverBox == this.prevDesign) {
                g.setColor(SystemPanel.yellowText);
            }
            else {
                g.setColor(Color.black);
            }
            g.fillPolygon(this.leftButtonX, this.leftButtonY, 3);
            if (this.hoverBox == this.nextDesign) {
                g.setColor(SystemPanel.yellowText);
            }
            else {
                g.setColor(Color.black);
            }
            g.fillPolygon(this.rightButtonX, this.rightButtonY, 3);
            final int barX = x + ColonyShipPane.s12;
            final int barW = w - ColonyShipPane.s24;
            final int barY = y + ColonyShipPane.s5;
            final int barH = h - ColonyShipPane.s10;
            g.setColor(Color.black);
            g.fillRect(barX, barY, barW, barH);
            this.shipNameBox.setBounds(barX, barY, barW, barH);
            g.setColor(PlanetsUI.sliderBoxBlue);
            g.setFont(this.narrowFont(18));
            final String name = c.shipyard().design().name();
            final int sw = g.getFontMetrics().stringWidth(name);
            final int x2 = barX + (barW - sw) / 2;
            g.drawString(name, x2, barY + ColonyShipPane.s16);
            if (this.hoverBox == this.shipNameBox) {
                final Stroke prev = g.getStroke();
                g.setColor(SystemPanel.yellowText);
                g.setStroke(ColonyShipPane.stroke2);
                g.draw(this.shipNameBox);
                g.setStroke(prev);
            }
        }
        
        private Image initializedBackgroundImage(final int w, final int h) {
            if (this.starBackground == null || this.starBackground.getWidth() != w || this.starBackground.getHeight() != h) {
                this.starBackground = new BufferedImage(w, h, 2);
                final Graphics g = this.starBackground.getGraphics();
                this.drawBackgroundStars(g, w, h);
                g.dispose();
            }
            return this.starBackground;
        }
        
        public void nextShipDesign(final boolean click) {
            final StarSystem sys = this.parent.systemViewToDisplay();
            final Colony c = (sys == null) ? null : sys.colony();
            if (c == null) {
                return;
            }
            if (!c.shipyard().canCycleDesign()) {
                if (click) {
                    this.misClick();
                }
            }
            else {
                if (click) {
                    this.softClick();
                }
                c.shipyard().goToNextDesign();
                this.parent.repaint();
            }
        }
        
        public void prevShipDesign(final boolean click) {
            final StarSystem sys = this.parent.systemViewToDisplay();
            final Colony c = (sys == null) ? null : sys.colony();
            if (c == null) {
                return;
            }
            if (!c.shipyard().canCycleDesign()) {
                if (click) {
                    this.misClick();
                }
            }
            else {
                if (click) {
                    this.softClick();
                }
                c.shipyard().goToPrevDesign();
                this.parent.repaint();
            }
        }
        
        @Override
        public void mouseClicked(final MouseEvent arg0) {
        }
        
        @Override
        public void mouseEntered(final MouseEvent arg0) {
        }
        
        @Override
        public void mouseExited(final MouseEvent arg0) {
            if (this.hoverBox != null) {
                this.hoverBox = null;
                this.repaint();
            }
        }
        
        @Override
        public void mousePressed(final MouseEvent arg0) {
        }
        
        @Override
        public void mouseReleased(final MouseEvent e) {
            if (e.getButton() > 3) {
                return;
            }
            final int x = e.getX();
            final int y = e.getY();
            if (this.shipDesignBox.contains(x, y)) {
                this.nextShipDesign(true);
                this.parent.repaint();
            }
            else if (this.shipNameBox.contains(x, y)) {
                this.nextShipDesign(true);
                this.parent.repaint();
            }
            else if (this.nextDesign.contains(x, y)) {
                this.nextShipDesign(true);
                this.parent.repaint();
            }
            else if (this.prevDesign.contains(x, y)) {
                this.prevShipDesign(true);
                this.parent.repaint();
            }
        }
        
        @Override
        public void mouseDragged(final MouseEvent arg0) {
        }
        
        @Override
        public void mouseMoved(final MouseEvent e) {
            final int x = e.getX();
            final int y = e.getY();
            final Shape prevHover = this.hoverBox;
            this.hoverBox = null;
            if (this.shipDesignBox.contains(x, y)) {
                this.hoverBox = this.shipDesignBox;
            }
            else if (this.shipNameBox.contains(x, y)) {
                this.hoverBox = this.shipNameBox;
            }
            else if (this.nextDesign.contains(x, y)) {
                this.hoverBox = this.nextDesign;
            }
            else if (this.prevDesign.contains(x, y)) {
                this.hoverBox = this.prevDesign;
            }
            if (prevHover != this.hoverBox) {
                this.repaint();
            }
        }
        
        @Override
        public void mouseWheelMoved(final MouseWheelEvent e) {
            if (e.getWheelRotation() < 0) {
                this.nextShipDesign(false);
            }
            else {
                this.prevShipDesign(false);
            }
            this.parent.repaint();
        }
    }
    
    class ColonyTransferFunds extends BasePanel implements MouseListener, MouseMotionListener
    {
        private static final long serialVersionUID = 7752365560363051366L;
        SystemPanel parent;
        private final Rectangle transferBox;
        private Shape hoverBox;
        private final Color textColor;
        
        ColonyTransferFunds(final SystemPanel p) {
            this.transferBox = new Rectangle();
            this.textColor = this.newColor(204, 204, 204);
            this.parent = p;
            this.init();
        }
        
        private void init() {
            this.setBackground(PlanetsUI.unselectedC);
            this.addMouseListener(this);
            this.addMouseMotionListener(this);
        }
        
        @Override
        public String textureName() {
            return this.parent.subPanelTextureName();
        }
        
        @Override
        public void paintComponent(final Graphics g0) {
            super.paintComponent(g0);
            final Graphics2D g = (Graphics2D)g0;
            final int w = this.getWidth();
            final int h = this.getHeight();
            int y0 = ColonyTransferFunds.s22;
            final int x0 = ColonyTransferFunds.s5;
            g.setFont(this.narrowFont(20));
            final String str = this.text("PLANETS_COLONY_TRANSFER_TITLE");
            this.drawShadowedString(g, str, 2, x0, y0, MainUI.shadeBorderC(), this.textColor);
            y0 += ColonyTransferFunds.s5;
            g.setFont(this.narrowFont(16));
            g.setColor(PlanetsUI.palette.black);
            final List<String> lines = this.scaledNarrowWrappedLines(g, this.text("PLANETS_COLONY_TRANSFER_DETAIL"), w - ColonyTransferFunds.s15, 2, 16, 12);
            for (final String line : lines) {
                y0 += ColonyTransferFunds.s16;
                g.drawString(line, x0, y0);
            }
            final int buttonH = ColonyTransferFunds.s30;
            this.drawTransferButton(g, ColonyTransferFunds.s5, h - buttonH - ColonyTransferFunds.s5, w - ColonyTransferFunds.s10, buttonH);
        }
        
        private void drawTransferButton(final Graphics2D g, final int x, final int y, final int w, final int h) {
            this.transferBox.setBounds(x, y, w, h);
            g.setColor(Color.blue);
            Stroke prev = g.getStroke();
            g.setStroke(ColonyTransferFunds.stroke3);
            g.setColor(Color.black);
            g.drawRect(x + ColonyTransferFunds.s2, y + ColonyTransferFunds.s2, w, h);
            g.setStroke(prev);
            g.setColor(PlanetsUI.unselectedC);
            g.fillRect(x, y, w, h);
            Color c0 = PlanetsUI.palette.white;
            if (this.hoverBox == this.transferBox && this.isButtonEnabled()) {
                c0 = Color.yellow;
            }
            final String lbl = this.text("PLANETS_COLONY_TRANSFER_BUTTON");
            final int fontSize = this.scaledFont(g, lbl, w - ColonyTransferFunds.s10, 20, 14);
            g.setFont(this.narrowFont(fontSize));
            final int sw = g.getFontMetrics().stringWidth(lbl);
            final int x2 = x + (w - sw) / 2;
            final int y2 = y + h - ColonyTransferFunds.s8;
            this.drawShadowedString(g, lbl, 2, x2, y2, MainUI.shadeBorderC(), c0);
            prev = g.getStroke();
            g.setStroke(ColonyTransferFunds.stroke2);
            g.setColor(c0);
            g.drawRect(x, y, w, h);
            g.setStroke(prev);
        }
        
        public boolean isButtonEnabled() {
            return (int)this.player().totalReserve() > 0;
        }
        
        public void buttonClicked() {
            if (this.isButtonEnabled()) {
                this.softClick();
                PlanetsUI.this.transferReservePane.targetSystem(PlanetsUI.this.selectedSystem());
                this.enableGlassPane(PlanetsUI.this.transferReservePane);
            }
            else {
                this.misClick();
            }
        }
        
        @Override
        public void mouseClicked(final MouseEvent arg0) {
        }
        
        @Override
        public void mouseEntered(final MouseEvent arg0) {
        }
        
        @Override
        public void mouseExited(final MouseEvent arg0) {
            if (this.hoverBox != null) {
                this.hoverBox = null;
                this.repaint();
            }
        }
        
        @Override
        public void mousePressed(final MouseEvent arg0) {
        }
        
        @Override
        public void mouseReleased(final MouseEvent e) {
            if (e.getButton() > 3) {
                return;
            }
            final int x = e.getX();
            final int y = e.getY();
            if (this.transferBox.contains(x, y)) {
                this.buttonClicked();
                this.parent.repaint();
            }
        }
        
        @Override
        public void mouseDragged(final MouseEvent arg0) {
        }
        
        @Override
        public void mouseMoved(final MouseEvent e) {
            final int x = e.getX();
            final int y = e.getY();
            final Shape prevHover = this.hoverBox;
            this.hoverBox = null;
            if (this.transferBox.contains(x, y)) {
                this.hoverBox = this.transferBox;
            }
            if (prevHover != this.hoverBox) {
                this.repaint();
            }
        }
    }
    
    final class PlanetDataListingUI extends SystemListingUI
    {
        PlanetDataListingUI(final BasePanel p) {
            super(p);
            this.setBorder(BorderFactory.createMatteBorder(this.scaled(3), 0, 0, 0, PlanetDataListingUI.selectedC));
        }
        
        @Override
        public String textureName() {
            return PlanetsUI.this.instance.subPanelTextureName();
        }
        
        @Override
        protected DataView dataView() {
            return PlanetsUI.this.views.get(PlanetsUI.selectedMode);
        }
        
        @Override
        protected List<StarSystem> systems() {
            return PlanetsUI.this.instance.systems();
        }
        
        @Override
        protected StarSystem selectedSystem() {
            return PlanetsUI.this.instance.selectedSystem();
        }
        
        @Override
        protected void selectedSystem(final StarSystem sys, final boolean updateFieldValues) {
            PlanetsUI.this.instance.selectedSystem(sys, updateFieldValues);
        }
        
        @Override
        protected void postInit() {
            PlanetsUI.nameField.addMouseListener(this);
            PlanetsUI.notesField.addMouseListener(this);
            this.add(PlanetsUI.nameField);
            this.add(PlanetsUI.notesField);
        }
    }
    
    class EmpireRevenueUI extends BasePanel
    {
        private static final long serialVersionUID = 5207706799726537551L;
        
        public EmpireRevenueUI() {
            this.initModel();
        }
        
        private void initModel() {
            this.setPreferredSize(new Dimension(this.getWidth(), this.scaled(160)));
            this.setOpaque(false);
            this.setLayout(new BorderLayout());
            this.add(new ReserveUI(), "East");
            this.add(new SpendingCostsUI(), "Center");
            this.add(new TotalIncomeUI(), "West");
        }
    }
    
    class SpendingCostsUI extends BasePanel
    {
        private static final long serialVersionUID = 6944948690925590061L;
        Shape textureClip;
        
        public SpendingCostsUI() {
            this.init();
        }
        
        private void init() {
            this.setOpaque(false);
        }
        
        @Override
        public String textureName() {
            return PlanetsUI.this.instance.subPanelTextureName();
        }
        
        @Override
        public Shape textureClip() {
            return this.textureClip;
        }
        
        @Override
        public void paintComponent(final Graphics g) {
            super.paintComponent(g);
            final int w = this.getWidth();
            final int h = this.getHeight();
            g.setFont(this.narrowFont(24));
            final String title = this.text("PLANETS_SPENDING_COSTS");
            int sw = g.getFontMetrics().stringWidth(title);
            final int x0 = (w - sw) / 2;
            this.drawShadowedString(g, title, 2, x0, SpendingCostsUI.s35, PlanetsUI.palette.black, SystemPanel.orangeText);
            final int margin = SpendingCostsUI.s10;
            int x2;
            final int border = x2 = SpendingCostsUI.s10;
            int w2 = w - x2 - border;
            int y1 = SpendingCostsUI.s45;
            final int h2 = h - y1 - SpendingCostsUI.s10;
            g.setColor(PlanetsUI.palette.medBack);
            g.fillRect(x2, y1, w2, h2);
            this.textureClip = new Rectangle(x2, y1, w2, h2);
            g.setFont(this.narrowFont(15));
            final String desc = this.text("PLANETS_COSTS_DESC");
            final List<String> descLines = this.wrappedLines(g, desc, w - SpendingCostsUI.s40);
            g.setColor(PlanetsUI.palette.black);
            y1 = SpendingCostsUI.s63;
            for (final String line : descLines) {
                g.drawString(line, x2 + SpendingCostsUI.s20, y1);
                y1 += SpendingCostsUI.s16;
            }
            g.setFont(this.narrowFont(20));
            w2 = w * 3 / 5 - x2 - SpendingCostsUI.s5;
            y1 = h - SpendingCostsUI.s50;
            int midX = x2 + w2 - SpendingCostsUI.s50;
            String lbl = this.text("PLANETS_COSTS_SHIPS");
            sw = g.getFontMetrics().stringWidth(lbl);
            this.drawShadowedString(g, lbl, 2, midX - sw, y1, SystemPanel.textShadowC, SystemPanel.whiteText);
            String val = this.text("PLANETS_AMT_PCT", this.fmt(100.0f * this.player().shipMaintCostPerBC(), 1));
            sw = g.getFontMetrics().stringWidth(val);
            g.setColor(PlanetsUI.palette.black);
            g.drawString(val, midX + SpendingCostsUI.s50 - sw, y1);
            y1 = h - SpendingCostsUI.s25;
            lbl = this.text("PLANETS_COSTS_BASES");
            sw = g.getFontMetrics().stringWidth(lbl);
            this.drawShadowedString(g, lbl, 2, midX - sw, y1, SystemPanel.textShadowC, SystemPanel.whiteText);
            val = this.text("PLANETS_AMT_PCT", this.fmt(100.0f * this.player().totalMissileBaseCostPct(), 1));
            sw = g.getFontMetrics().stringWidth(val);
            g.setColor(PlanetsUI.palette.black);
            g.drawString(val, midX + SpendingCostsUI.s50 - sw, y1);
            x2 = w * 2 / 3 + SpendingCostsUI.s20;
            w2 = w - x2 - border - margin;
            y1 = h - SpendingCostsUI.s50;
            midX = x2 + w2 - SpendingCostsUI.s50;
            lbl = this.text("PLANETS_COSTS_SPYING");
            sw = g.getFontMetrics().stringWidth(lbl);
            this.drawShadowedString(g, lbl, 2, midX - sw, y1, SystemPanel.textShadowC, SystemPanel.whiteText);
            val = this.text("PLANETS_AMT_PCT", this.fmt(100.0f * this.player().totalSpyCostPct(), 1));
            sw = g.getFontMetrics().stringWidth(val);
            g.setColor(PlanetsUI.palette.black);
            g.drawString(val, midX + SpendingCostsUI.s50 - sw, y1);
            y1 = h - SpendingCostsUI.s25;
            lbl = this.text("PLANETS_COSTS_SECURITY");
            sw = g.getFontMetrics().stringWidth(lbl);
            this.drawShadowedString(g, lbl, 2, midX - sw, y1, SystemPanel.textShadowC, SystemPanel.whiteText);
            val = this.text("PLANETS_AMT_PCT", this.fmt(100.0f * this.player().internalSecurityCostPct(), 1));
            sw = g.getFontMetrics().stringWidth(val);
            g.setColor(PlanetsUI.palette.black);
            g.drawString(val, midX + SpendingCostsUI.s50 - sw, y1);
        }
    }
    
    class TotalIncomeUI extends BasePanel
    {
        private static final long serialVersionUID = 142712456623960298L;
        Shape textureClip;
        
        public TotalIncomeUI() {
            this.initModel();
        }
        
        private void initModel() {
            this.setPreferredSize(new Dimension(this.scaled(300), this.getHeight()));
            this.setOpaque(false);
        }
        
        @Override
        public String textureName() {
            return PlanetsUI.this.instance.subPanelTextureName();
        }
        
        @Override
        public Shape textureClip() {
            return this.textureClip;
        }
        
        @Override
        public void paintComponent(final Graphics g) {
            super.paintComponent(g);
            final int w = this.getWidth();
            final int h = this.getHeight();
            g.setFont(this.narrowFont(24));
            final String title = this.text("PLANETS_TOTAL_INCOME");
            int sw = g.getFontMetrics().stringWidth(title);
            final int x0 = (w - sw) / 2;
            this.drawShadowedString(g, title, 2, x0, TotalIncomeUI.s35, PlanetsUI.palette.black, SystemPanel.orangeText);
            final int margin = TotalIncomeUI.s10;
            int x2;
            final int border = x2 = TotalIncomeUI.s10;
            int w2 = w - x2 - border;
            int y1 = TotalIncomeUI.s45;
            final int h2 = h - y1 - TotalIncomeUI.s10;
            g.setColor(PlanetsUI.palette.medBack);
            g.fillRect(x2, y1, w2, h2);
            this.textureClip = new Rectangle(x2, y1, w2, h2);
            final int midP = x2 + w2 * 3 / 5;
            final int amtP = midP + TotalIncomeUI.s90;
            x2 += margin;
            w2 -= 2 * margin;
            y1 += TotalIncomeUI.s25;
            g.setFont(this.narrowFont(20));
            String label = this.text("PLANETS_INCOME_TRADE");
            sw = g.getFontMetrics().stringWidth(label);
            this.drawShadowedString(g, label, 2, midP - sw, y1, SystemPanel.textShadowC, SystemPanel.whiteText);
            String val = this.text("PLANETS_AMT_BC", TotalIncomeUI.df1.format(this.player().netTradeIncome()));
            sw = g.getFontMetrics().stringWidth(val);
            g.setColor(PlanetsUI.palette.black);
            g.drawString(val, amtP - sw, y1);
            y1 += TotalIncomeUI.s25;
            label = this.text("PLANETS_INCOME_PLANETS");
            sw = g.getFontMetrics().stringWidth(label);
            this.drawShadowedString(g, label, 2, midP - sw, y1, SystemPanel.textShadowC, SystemPanel.whiteText);
            val = this.text("PLANETS_AMT_BC", TotalIncomeUI.df1.format(this.player().totalPlanetaryIncome()));
            sw = g.getFontMetrics().stringWidth(val);
            g.setColor(PlanetsUI.palette.black);
            g.drawString(val, amtP - sw, y1);
            y1 += TotalIncomeUI.s15;
            g.setColor(Color.black);
            g.fillRect(x2, y1, w2, TotalIncomeUI.s3 / 2);
            y1 += TotalIncomeUI.s25;
            label = this.text("PLANETS_INCOME_TOTAL");
            sw = g.getFontMetrics().stringWidth(label);
            this.drawShadowedString(g, label, 2, midP - sw, y1, SystemPanel.textShadowC, SystemPanel.whiteText);
            val = this.text("PLANETS_AMT_BC", TotalIncomeUI.df1.format(this.player().totalIncome()));
            sw = g.getFontMetrics().stringWidth(val);
            g.setColor(PlanetsUI.palette.black);
            g.drawString(val, amtP - sw, y1);
        }
    }
    
    class ReserveUI extends BasePanel implements MouseListener, MouseMotionListener, MouseWheelListener
    {
        private static final long serialVersionUID = -6332370829725938082L;
        final Color sliderHighlightColor;
        final Color sliderBoxEnabled;
        final Color sliderBackEnabled;
        private final Polygon leftArrow;
        private final Polygon rightArrow;
        private final Rectangle sliderBox;
        private Shape hoverBox;
        private final int[] leftButtonX;
        private final int[] leftButtonY;
        private final int[] rightButtonX;
        private final int[] rightButtonY;
        Shape textureClip;
        
        public ReserveUI() {
            this.sliderHighlightColor = new Color(255, 255, 255);
            this.sliderBoxEnabled = new Color(34, 140, 142);
            this.sliderBackEnabled = Color.black;
            this.leftArrow = new Polygon();
            this.rightArrow = new Polygon();
            this.sliderBox = new Rectangle();
            this.leftButtonX = new int[3];
            this.leftButtonY = new int[3];
            this.rightButtonX = new int[3];
            this.rightButtonY = new int[3];
            this.initModel();
        }
        
        private void initModel() {
            this.setOpaque(false);
            this.setPreferredSize(new Dimension(this.scaled(300), this.getHeight()));
            this.addMouseListener(this);
            this.addMouseMotionListener(this);
            this.addMouseWheelListener(this);
        }
        
        @Override
        public String textureName() {
            return PlanetsUI.this.instance.subPanelTextureName();
        }
        
        @Override
        public Shape textureClip() {
            return this.textureClip;
        }
        
        @Override
        public void paintComponent(final Graphics g0) {
            super.paintComponent(g0);
            final Graphics2D g = (Graphics2D)g0;
            final int w = this.getWidth();
            final int h = this.getHeight();
            g.setFont(this.narrowFont(24));
            final String title = this.text("PLANETS_TREASURY");
            int sw = g.getFontMetrics().stringWidth(title);
            final int x0 = (w - sw) / 2;
            this.drawShadowedString(g, title, 2, x0, ReserveUI.s35, PlanetsUI.palette.black, SystemPanel.orangeText);
            int x2;
            final int border = x2 = ReserveUI.s10;
            final int w2 = w - x2 - border;
            int y1 = ReserveUI.s45;
            final int h2 = h - y1 - ReserveUI.s10;
            g.setColor(PlanetsUI.palette.medBack);
            g.fillRect(x2, y1, w2, h2);
            this.textureClip = new Rectangle(x2, y1, w2, h2);
            final int midP = x2 + w2 * 3 / 5;
            y1 += ReserveUI.s20;
            g.setFont(this.narrowFont(20));
            final String label = this.text("PLANETS_TREASURY_FUNDS");
            sw = g.getFontMetrics().stringWidth(label);
            this.drawShadowedString(g, label, 2, midP - sw, y1, SystemPanel.textShadowC, SystemPanel.whiteText);
            g.setColor(PlanetsUI.palette.black);
            final String text = this.text("PLANETS_AMT_BC", (int)this.player().totalReserve());
            g.drawString(text, midP + ReserveUI.s10, y1);
            y1 += ReserveUI.s10;
            final List<String> lines = this.scaledNarrowWrappedLines(g, this.text("PLANETS_TAX_DESC"), w - ReserveUI.s40, 1, 15, 12);
            for (final String line : lines) {
                y1 += ReserveUI.s15;
                g.drawString(line, ReserveUI.s20, y1);
            }
            y1 += ReserveUI.s30;
            x2 = ReserveUI.s20;
            g.setFont(this.narrowFont(16));
            final String lbl = this.text("PLANETS_RESERVE_TAX");
            sw = g.getFontMetrics().stringWidth(lbl);
            g.drawString(lbl, x2, y1);
            x2 = x2 + sw + ReserveUI.s5;
            final int boxW = ReserveUI.s100;
            this.drawSliderBox(g, x2, y1 - ReserveUI.s15, boxW, ReserveUI.s18);
            String result;
            if (this.player().empireTaxLevel() > 0) {
                result = this.text("PLANETS_RESERVE_INCREASE", this.fmt(this.player().empireTaxRevenue(), 1));
            }
            else {
                result = this.text("PLANETS_RESERVE_NO_TAX");
            }
            g.setFont(this.narrowFont(16));
            g.setColor(PlanetsUI.palette.black);
            x2 = x2 + boxW + ReserveUI.s5;
            g.drawString(result, x2, y1);
        }
        
        private void drawSliderBox(final Graphics2D g, final int x, final int y, final int w, final int h) {
            final int leftMargin = x;
            final int rightMargin = x + w;
            final int buttonW = ReserveUI.s10;
            final int buttonBottomY = y + h;
            final int buttonTopY = y;
            final int buttonMidY = (buttonTopY + buttonBottomY) / 2;
            final int boxBorderW = ReserveUI.s3;
            final int boxL = x + ReserveUI.s10;
            final int boxTopY = y;
            final int boxW = w - ReserveUI.s20;
            final int boxH = h;
            this.leftButtonX[0] = leftMargin;
            this.leftButtonX[1] = leftMargin + buttonW;
            this.leftButtonX[2] = leftMargin + buttonW;
            this.leftButtonY[0] = buttonMidY;
            this.leftButtonY[1] = buttonTopY;
            this.leftButtonY[2] = buttonBottomY;
            this.rightButtonX[0] = rightMargin;
            this.rightButtonX[1] = rightMargin - buttonW;
            this.rightButtonX[2] = rightMargin - buttonW;
            this.rightButtonY[0] = buttonMidY;
            this.rightButtonY[1] = buttonTopY;
            this.rightButtonY[2] = buttonBottomY;
            final Color c1 = this.sliderBoxEnabled;
            final Color c2 = this.sliderBackEnabled;
            Color c3 = (this.hoverBox == this.leftArrow) ? SystemPanel.yellowText : this.sliderBackEnabled;
            g.setColor(c3);
            g.fillPolygon(this.leftButtonX, this.leftButtonY, 3);
            c3 = ((this.hoverBox == this.rightArrow) ? SystemPanel.yellowText : this.sliderBackEnabled);
            g.setColor(c3);
            g.fillPolygon(this.rightButtonX, this.rightButtonY, 3);
            this.leftArrow.reset();
            this.rightArrow.reset();
            for (int i = 0; i < this.leftButtonX.length; ++i) {
                this.leftArrow.addPoint(this.leftButtonX[i], this.leftButtonY[i]);
                this.rightArrow.addPoint(this.rightButtonX[i], this.rightButtonY[i]);
            }
            this.sliderBox.x = boxL;
            this.sliderBox.y = boxTopY;
            this.sliderBox.width = boxW;
            this.sliderBox.height = boxH;
            g.setFont(this.narrowFont(18));
            final String pctAmt = this.text("PLANETS_AMT_PCT", this.player().empireTaxLevel());
            final int sw = g.getFontMetrics().stringWidth(pctAmt);
            final int y2 = boxTopY + boxH - ReserveUI.s3;
            final int x2 = this.sliderBox.x + (this.sliderBox.width - sw) / 2;
            g.setColor(c2);
            int coloredW = boxW - 2 * boxBorderW;
            g.setClip(boxL + boxBorderW, boxTopY, coloredW, boxH);
            g.fillRect(boxL + boxBorderW, boxTopY, coloredW, boxH);
            if (this.player().empireTaxLevel() > 0) {
                if (this.player().empireTaxLevel() < this.player().maxEmpireTaxLevel()) {
                    coloredW = coloredW * this.player().empireTaxLevel() / this.player().maxEmpireTaxLevel();
                    g.setClip(boxL + boxBorderW, boxTopY, coloredW, boxH);
                }
                g.setColor(c1);
                g.fillRect(boxL + boxBorderW, boxTopY + ReserveUI.s2, boxW - 2 * boxBorderW, boxH - ReserveUI.s3);
            }
            g.setClip(null);
            g.setColor(PlanetsUI.palette.white);
            g.drawString(pctAmt, x2, y2);
            if (this.hoverBox == this.sliderBox) {
                g.setColor(SystemPanel.yellowText);
                final Stroke prev = g.getStroke();
                g.setStroke(ReserveUI.stroke2);
                g.drawRect(boxL + ReserveUI.s3, boxTopY + ReserveUI.s1, boxW - ReserveUI.s6, boxH - ReserveUI.s2);
                g.setStroke(prev);
            }
        }
        
        public void decrement(final boolean click) {
            if (this.player().decrementEmpireTaxLevel()) {
                if (click) {
                    this.softClick();
                }
                this.repaint();
                PlanetsUI.this.planetDisplayPane.repaint();
            }
            else if (click) {
                this.misClick();
            }
        }
        
        public void increment(final boolean click) {
            if (this.player().incrementEmpireTaxLevel()) {
                if (click) {
                    this.softClick();
                }
                this.repaint();
                PlanetsUI.this.planetDisplayPane.repaint();
            }
            else if (click) {
                this.misClick();
            }
        }
        
        public float pctBoxSelected(final int x, final int y) {
            final int bw = ReserveUI.s3;
            final int minX = this.sliderBox.x + bw;
            final int maxX = this.sliderBox.x + this.sliderBox.width - bw;
            if (x < minX || x > maxX || y < this.sliderBox.y + bw || y > this.sliderBox.y + this.sliderBox.height - bw) {
                return -1.0f;
            }
            final float num = (float)(x - minX);
            final float den = (float)(maxX - minX);
            return num / den;
        }
        
        @Override
        public void mouseClicked(final MouseEvent e) {
        }
        
        @Override
        public void mousePressed(final MouseEvent e) {
        }
        
        @Override
        public void mouseReleased(final MouseEvent e) {
            if (e.getButton() > 3) {
                return;
            }
            final int x = e.getX();
            final int y = e.getY();
            if (this.leftArrow.contains(x, y)) {
                this.decrement(true);
            }
            else if (this.rightArrow.contains(x, y)) {
                this.increment(true);
            }
            else {
                final float pct = this.pctBoxSelected(x, y);
                if (pct >= 0.0f) {
                    final int newLevel = (int)Math.ceil(pct * this.player().maxEmpireTaxLevel());
                    this.softClick();
                    this.player().empireTaxLevel(newLevel);
                    this.repaint();
                    PlanetsUI.this.planetDisplayPane.repaint();
                }
            }
        }
        
        @Override
        public void mouseEntered(final MouseEvent e) {
        }
        
        @Override
        public void mouseExited(final MouseEvent e) {
            if (this.hoverBox != null) {
                this.hoverBox = null;
                this.repaint();
            }
        }
        
        @Override
        public void mouseDragged(final MouseEvent e) {
        }
        
        @Override
        public void mouseMoved(final MouseEvent e) {
            final int x = e.getX();
            final int y = e.getY();
            Shape newHover = null;
            if (this.sliderBox.contains(x, y)) {
                newHover = this.sliderBox;
            }
            else if (this.leftArrow.contains(x, y)) {
                newHover = this.leftArrow;
            }
            else if (this.rightArrow.contains(x, y)) {
                newHover = this.rightArrow;
            }
            if (newHover != this.hoverBox) {
                this.hoverBox = newHover;
                this.repaint();
            }
        }
        
        @Override
        public void mouseWheelMoved(final MouseWheelEvent e) {
            final int rot = e.getWheelRotation();
            if (this.hoverBox == this.sliderBox) {
                if (rot > 0) {
                    this.decrement(false);
                }
                else if (rot < 0) {
                    this.increment(false);
                }
            }
        }
    }
    
    class ExitPlanetsButton extends ExitButton
    {
        public ExitPlanetsButton(final int w, final int h, final int vMargin, final int hMargin) {
            super(w, h, vMargin, hMargin);
        }
        
        @Override
        protected void clickAction(final int numClicks) {
            PlanetsUI.this.displayedSystems = null;
            PlanetsUI.this.finish(true);
        }
    }
    
    class UpAction extends AbstractAction
    {
        private static final long serialVersionUID = 7919574292147534022L;
        
        @Override
        public void actionPerformed(final ActionEvent ev) {
            if (PlanetsUI.this.listingUI.scrollUp()) {
                PlanetsUI.this.instance.repaint();
            }
        }
    }
    
    class DownAction extends AbstractAction
    {
        private static final long serialVersionUID = 389828827499253980L;
        
        @Override
        public void actionPerformed(final ActionEvent ev) {
            if (PlanetsUI.this.listingUI.scrollDown()) {
                PlanetsUI.this.instance.repaint();
            }
        }
    }
    
    class CancelAction extends AbstractAction
    {
        private static final long serialVersionUID = 5743741599120107395L;
        
        @Override
        public void actionPerformed(final ActionEvent ev) {
            PlanetsUI.this.displayedSystems = null;
            PlanetsUI.this.setFieldValues(PlanetsUI.this.selectedSystem());
            PlanetsUI.this.finish(false);
        }
    }
}
