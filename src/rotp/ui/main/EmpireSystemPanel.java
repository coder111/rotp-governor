// 
// Decompiled by Procyon v0.5.36
// 

package rotp.ui.main;

import java.awt.event.MouseWheelEvent;
import rotp.model.Sprite;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.Paint;
import java.awt.GradientPaint;
import java.awt.Image;
import rotp.model.ships.Design;
import java.awt.Stroke;
import java.awt.image.ImageObserver;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Shape;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.MouseWheelListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseListener;
import javax.swing.JPanel;
import java.awt.Component;
import rotp.ui.SystemViewer;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.BorderLayout;
import rotp.ui.BasePanel;
import rotp.model.colony.Colony;
import rotp.model.galaxy.StarSystem;
import java.awt.event.KeyEvent;
import java.awt.Container;
import java.awt.Color;

public class EmpireSystemPanel extends SystemPanel
{
    private static final long serialVersionUID = -9095582389365709013L;
    static final Color sliderButtonColor;
    static final Color sliderHighlightColor;
    static final Color spendingPaneHighlightColor;
    static final Color productionGreenColor;
    static final Color darkBrown;
    static final Color brown;
    static final Color darkText;
    static final Color sliderBoxBlue;
    private SystemViewInfoPane topPane;
    private EmpireColonySpendingPane spendingPane;
    private EmpireShipPane shipPane;
    
    public EmpireSystemPanel(final SpriteDisplayPanel p) {
        this.parentSpritePanel = p;
        this.init();
    }
    
    private void init() {
        this.initModel();
    }
    
    @Override
    public String subPanelTextureName() {
        return "TEXTURE_GRAY";
    }
    
    @Override
    protected void showDefaultDetail() {
        this.detailLayout.show(this.detailCardPane, "EMPIRE_DETAIL");
    }
    
    @Override
    protected void showStarDetail() {
        this.detailLayout.show(this.detailCardPane, "STAR_DETAIL");
    }
    
    @Override
    protected void showPlanetDetail() {
        this.detailLayout.show(this.detailCardPane, "PLANET_DETAIL");
    }
    
    @Override
    public void animate() {
        this.topPane.animate();
    }
    
    @Override
    public void keyPressed(final KeyEvent e) {
        final int k = e.getKeyCode();
        switch (k) {
            case 83: {
                this.nextShipDesign();
            }
            case KeyEvent.VK_Q:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53: {
                this.spendingPane.keyPressed(e);
                break;
            }
        }
    }
    
    public void nextShipDesign() {
        final StarSystem sys = this.parentSpritePanel.systemViewToDisplay();
        if (sys == null) {
            return;
        }
        final Colony c = sys.colony();
        if (c == null) {
            return;
        }
        if (!c.shipyard().canCycleDesign()) {
            this.misClick();
        }
        else {
            this.softClick();
            c.shipyard().goToNextDesign();
            this.parentSpritePanel.repaint();
        }
    }
    
    public void prevShipDesign() {
        final StarSystem sys = this.parentSpritePanel.systemViewToDisplay();
        if (sys == null) {
            return;
        }
        final Colony c = sys.colony();
        if (c == null) {
            return;
        }
        if (!c.shipyard().canCycleDesign()) {
            this.misClick();
        }
        else {
            this.softClick();
            c.shipyard().goToPrevDesign();
            this.parentSpritePanel.repaint();
        }
    }
    
    @Override
    protected BasePanel topPane() {
        if (this.topPane == null) {
            this.topPane = new SystemViewInfoPane(this);
        }
        return this.topPane;
    }
    
    @Override
    protected BasePanel detailPane() {
        final BasePanel detailTopPane = new BasePanel();
        detailTopPane.setOpaque(true);
        detailTopPane.setBackground(EmpireSystemPanel.dataBorders);
        final BorderLayout layout = new BorderLayout();
        layout.setVgap(EmpireSystemPanel.s1);
        detailTopPane.setLayout(layout);
        detailTopPane.setPreferredSize(new Dimension(this.getWidth(), this.scaled(110)));
        detailTopPane.add(new EmpireColonyFoundedPane(this, MainUI.paneBackground()), "North");
        detailTopPane.add(new EmpireColonyInfoPane(this, MainUI.paneBackground(), EmpireSystemPanel.dataBorders, SystemPanel.yellowText, EmpireSystemPanel.darkText), "Center");
        final Color textC = new Color(204, 204, 204);
        this.spendingPane = new EmpireColonySpendingPane(this, MainUI.paneBackground(), textC, EmpireSystemPanel.labelBorderHi, EmpireSystemPanel.labelBorderLo);
        this.shipPane = new EmpireShipPane(this);
        final BasePanel empireDetailPane = new BasePanel();
        empireDetailPane.setOpaque(false);
        final BorderLayout layout2 = new BorderLayout();
        empireDetailPane.setLayout(layout2);
        empireDetailPane.add(detailTopPane, "North");
        empireDetailPane.add(this.spendingPane, "Center");
        empireDetailPane.add(this.shipPane, "South");
        (this.detailCardPane = new JPanel()).setOpaque(false);
        this.detailCardPane.setLayout(this.detailLayout);
        this.detailCardPane.add(empireDetailPane, "EMPIRE_DETAIL");
        this.detailLayout.show(this.detailCardPane, "EMPIRE_DETAIL");
        final BasePanel pane = new BasePanel();
        pane.setLayout(new BorderLayout());
        pane.setBackground(MainUI.paneBackground());
        pane.add(this.detailCardPane, "Center");
        return pane;
    }
    
    static {
        sliderButtonColor = new Color(153, 0, 11);
        sliderHighlightColor = new Color(255, 255, 255);
        spendingPaneHighlightColor = new Color(96, 96, 224);
        productionGreenColor = new Color(89, 240, 46);
        darkBrown = new Color(45, 14, 5);
        brown = new Color(64, 24, 13);
        darkText = new Color(40, 40, 40);
        sliderBoxBlue = new Color(34, 140, 142);
    }
    
    class EmpireShipPane extends BasePanel implements MouseListener, MouseMotionListener, MouseWheelListener
    {
        private static final long serialVersionUID = 7752365560363051366L;
        private final EmpireSystemPanel parent;
        private final int[] leftButtonX;
        private final int[] leftButtonY;
        private final int[] rightButtonX;
        private final int[] rightButtonY;
        private final Rectangle shipDesignBox;
        private final Rectangle rallyPointBox;
        private final Rectangle transportBox;
        private final Rectangle shipNameBox;
        private final Polygon prevDesign;
        private final Polygon nextDesign;
        private Shape hoverBox;
        Color textColor;
        Color gray20C;
        Color darkShadingC;
        Color buttonC;
        Color gray70C;
        Color gray90C;
        Color gray115C;
        Color gray150C;
        Color gray175C;
        Color gray190C;
        
        EmpireShipPane(final EmpireSystemPanel p) {
            this.leftButtonX = new int[3];
            this.leftButtonY = new int[3];
            this.rightButtonX = new int[3];
            this.rightButtonY = new int[3];
            this.shipDesignBox = new Rectangle();
            this.rallyPointBox = new Rectangle();
            this.transportBox = new Rectangle();
            this.shipNameBox = new Rectangle();
            this.prevDesign = new Polygon();
            this.nextDesign = new Polygon();
            this.textColor = this.newColor(204, 204, 204);
            this.gray20C = this.newColor(20, 20, 20);
            this.darkShadingC = this.newColor(50, 50, 50);
            this.buttonC = this.newColor(110, 110, 110);
            this.gray70C = this.newColor(70, 70, 70);
            this.gray90C = this.newColor(90, 90, 90);
            this.gray115C = this.newColor(115, 115, 115);
            this.gray150C = this.newColor(150, 150, 150);
            this.gray175C = this.newColor(175, 175, 175);
            this.gray190C = this.newColor(190, 190, 190);
            this.parent = p;
            this.init();
        }
        
        private void init() {
            this.setBackground(MainUI.paneBackground());
            this.setPreferredSize(new Dimension(this.getWidth(), this.scaled(150)));
            this.addMouseListener(this);
            this.addMouseMotionListener(this);
            this.addMouseWheelListener(this);
        }
        
        @Override
        public String textureName() {
            return "TEXTURE_GRAY";
        }
        
        @Override
        public void paintComponent(final Graphics g0) {
            super.paintComponent(g0);
            final Graphics2D g = (Graphics2D)g0;
            final int w = this.getWidth();
            final int h = this.getHeight();
            final int midMargin = this.scaled(105);
            final StarSystem sys = EmpireSystemPanel.this.parentSpritePanel.systemViewToDisplay();
            final Colony col = (sys == null) ? null : sys.colony();
            if (col == null) {
                return;
            }
            this.drawTitle(g);
            this.drawShipIcon(g, col, EmpireShipPane.s5, EmpireShipPane.s30, midMargin - EmpireShipPane.s15, EmpireShipPane.s75);
            this.drawShipCompletion(g, col, midMargin, h - this.scaled(116), w - EmpireShipPane.s10 - midMargin, EmpireShipPane.s30);
            this.drawNameSelector(g, col, midMargin, h - this.scaled(103), w - EmpireShipPane.s10 - midMargin, EmpireShipPane.s30);
            this.drawRallyPointButton(g, midMargin, h - EmpireShipPane.s70, w - EmpireShipPane.s10 - midMargin, EmpireShipPane.s25);
            this.drawTransportButton(g, 0, h - EmpireShipPane.s35, w, EmpireShipPane.s35);
        }
        
        private void drawTitle(final Graphics g) {
            g.setColor(MainUI.shadeBorderC());
            g.fillRect(0, 0, this.getWidth(), EmpireShipPane.s3);
            g.setFont(this.narrowFont(20));
            g.setColor(Color.black);
            final String str = this.text("MAIN_COLONY_SHIPYARD_CONSTRUCTION");
            this.drawShadowedString(g, str, 2, EmpireShipPane.s5, EmpireShipPane.s22, MainUI.shadeBorderC(), this.textColor);
        }
        
        private void drawShipIcon(final Graphics2D g, final Colony c, final int x, final int y, final int w, final int h) {
            g.setColor(Color.black);
            g.fillRect(x, y, w, h);
            this.shipDesignBox.setBounds(x, y, w, h);
            g.drawImage(this.initializedBackgroundImage(w, h), x, y, null);
            final int y0a = h / 3;
            final int y0b = h * 2 / 3;
            g.setColor(EmpireSystemPanel.darkBrown);
            g.fillRect(x, y + EmpireShipPane.s4, w, EmpireShipPane.s4);
            g.fillRect(x, y + h - EmpireShipPane.s8, w, EmpireShipPane.s4);
            g.fillRect(x, y + y0a, w, EmpireShipPane.s4);
            g.fillRect(x, y + y0b - EmpireShipPane.s4, w, EmpireShipPane.s4);
            g.setColor(EmpireSystemPanel.brown);
            g.fillRect(x, y + EmpireShipPane.s8, w, EmpireShipPane.s4);
            g.fillRect(x, y + h - EmpireShipPane.s12, w, EmpireShipPane.s4);
            g.fillRect(x, y + y0a - EmpireShipPane.s4, w, EmpireShipPane.s4);
            g.fillRect(x, y + y0b, w, EmpireShipPane.s4);
            g.setColor(EmpireSystemPanel.darkBrown);
            g.fillRect(x + w / 8, y + y0a + EmpireShipPane.s4, EmpireShipPane.s4, y0b - y0a - EmpireShipPane.s8);
            g.fillRect(x + w * 3 / 8, y + y0a + EmpireShipPane.s4, EmpireShipPane.s4, y0b - y0a - EmpireShipPane.s8);
            g.fillRect(x + w * 5 / 8, y + y0a + EmpireShipPane.s4, EmpireShipPane.s4, y0b - y0a - EmpireShipPane.s8);
            g.fillRect(x + w * 7 / 8, y + y0a + EmpireShipPane.s4, EmpireShipPane.s4, y0b - y0a - EmpireShipPane.s8);
            final Stroke prevStroke = g.getStroke();
            g.setStroke(EmpireShipPane.stroke2);
            g.drawLine(x + EmpireShipPane.s4, y + EmpireShipPane.s12, x + w / 5 - EmpireShipPane.s4, y + y0a - EmpireShipPane.s4);
            g.drawLine(x + w / 5 + EmpireShipPane.s4, y + y0a - EmpireShipPane.s4, x + w * 2 / 5 - EmpireShipPane.s4, y + EmpireShipPane.s12);
            g.drawLine(x + w * 2 / 5 + EmpireShipPane.s4, y + EmpireShipPane.s12, x + w * 3 / 5 - EmpireShipPane.s4, y + y0a - EmpireShipPane.s4);
            g.drawLine(x + w * 3 / 5 + EmpireShipPane.s4, y + y0a - EmpireShipPane.s4, x + w * 4 / 5 - EmpireShipPane.s4, y + EmpireShipPane.s12);
            g.drawLine(x + w * 4 / 5 + EmpireShipPane.s4, y + EmpireShipPane.s12, x + w - EmpireShipPane.s4, y + y0a - EmpireShipPane.s4);
            g.drawLine(x + EmpireShipPane.s4, y + h - EmpireShipPane.s12, x + w / 5 - EmpireShipPane.s4, y + y0b + EmpireShipPane.s4);
            g.drawLine(x + w / 5 + EmpireShipPane.s4, y + y0b + EmpireShipPane.s4, x + w * 2 / 5 - EmpireShipPane.s4, y + h - EmpireShipPane.s12);
            g.drawLine(x + w * 2 / 5 + EmpireShipPane.s4, y + h - EmpireShipPane.s12, x + w * 3 / 5 - EmpireShipPane.s4, y + y0b + EmpireShipPane.s4);
            g.drawLine(x + w * 3 / 5 + EmpireShipPane.s4, y + y0b + 4, x + w * 4 / 5 - 4, y + h - 12);
            g.drawLine(x + w * 4 / 5 + EmpireShipPane.s4, y + h - EmpireShipPane.s12, x + w - EmpireShipPane.s4, y + y0b + EmpireShipPane.s4);
            g.setStroke(prevStroke);
            if (c == null) {
                return;
            }
            final Design d = c.shipyard().design();
            final Image img = d.image();
            if (img == null) {
                return;
            }
            final int w2 = img.getWidth(null);
            final int h2 = img.getHeight(null);
            final float scale = this.min(w / (float)w2, h / (float)h2);
            final int w3 = (int)(scale * w2);
            final int h3 = (int)(scale * h2);
            final int x2 = x + (w - w3) / 2;
            final int y2 = y + (h - h3) / 2;
            g.drawImage(img, x2, y2, x2 + w3, y2 + h3, 0, 0, w2, h2, this);
            if (this.hoverBox == this.shipDesignBox) {
                final Stroke prev = g.getStroke();
                g.setStroke(EmpireShipPane.stroke2);
                g.setColor(SystemPanel.yellowText);
                g.drawRect(x, y, w, h);
                g.setStroke(prev);
            }
            String count = null;
            if (d.scrapped()) {
                count = this.text("MAIN_COLONY_SHIP_SCRAPPED");
                g.setColor(Color.red);
                g.setFont(this.narrowFont(20));
            }
            else {
                final int i = c.shipyard().upcomingShipCount();
                if (i == 0) {
                    return;
                }
                count = Integer.toString(i);
                g.setColor(SystemPanel.yellowText);
                g.setFont(this.narrowFont(20));
            }
            final int sw = g.getFontMetrics().stringWidth(count);
            g.drawString(count, x + w - EmpireShipPane.s5 - sw, y + h - EmpireShipPane.s5);
        }
        
        private void drawShipCompletion(final Graphics2D g, final Colony c, final int x, final int y, final int w, final int h) {
            if (c == null) {
                return;
            }
            final String result = c.shipyard().shipCompletionResult();
            g.setColor(Color.black);
            g.setFont(this.narrowFont(16));
            g.drawString(result, x + EmpireShipPane.s12, y + EmpireShipPane.s10);
        }
        
        private void drawNameSelector(final Graphics2D g, final Colony c, final int x, final int y, final int w, final int h) {
            final int leftM = x;
            final int rightM = x + w;
            final int buttonW = EmpireShipPane.s10;
            final int buttonTopY = y + EmpireShipPane.s5;
            final int buttonMidY = y + EmpireShipPane.s15;
            final int buttonBotY = y + EmpireShipPane.s25;
            this.leftButtonX[0] = leftM;
            this.leftButtonX[1] = leftM + buttonW;
            this.leftButtonX[2] = leftM + buttonW;
            this.leftButtonY[0] = buttonMidY;
            this.leftButtonY[1] = buttonTopY;
            this.leftButtonY[2] = buttonBotY;
            this.rightButtonX[0] = rightM;
            this.rightButtonX[1] = rightM - buttonW;
            this.rightButtonX[2] = rightM - buttonW;
            this.rightButtonY[0] = buttonMidY;
            this.rightButtonY[1] = buttonTopY;
            this.rightButtonY[2] = buttonBotY;
            this.prevDesign.reset();
            this.nextDesign.reset();
            for (int i = 0; i < this.leftButtonX.length; ++i) {
                this.prevDesign.addPoint(this.leftButtonX[i], this.leftButtonY[i]);
                this.nextDesign.addPoint(this.rightButtonX[i], this.rightButtonY[i]);
            }
            if (this.hoverBox == this.prevDesign) {
                g.setColor(SystemPanel.yellowText);
            }
            else {
                g.setColor(Color.black);
            }
            g.fillPolygon(this.leftButtonX, this.leftButtonY, 3);
            if (this.hoverBox == this.nextDesign) {
                g.setColor(SystemPanel.yellowText);
            }
            else {
                g.setColor(Color.black);
            }
            g.fillPolygon(this.rightButtonX, this.rightButtonY, 3);
            final int barX = x + EmpireShipPane.s12;
            final int barW = w - EmpireShipPane.s24;
            final int barY = y + EmpireShipPane.s5;
            final int barH = h - EmpireShipPane.s10;
            g.setColor(Color.black);
            g.fillRect(barX, barY, barW, barH);
            this.shipNameBox.setBounds(barX, barY, barW, barH);
            if (c != null) {
                g.setColor(EmpireSystemPanel.sliderBoxBlue);
                String name = c.shipyard().design().name();
                if (name == null) {
                    final int j = 0;
                    name = "";
                }
                this.scaledFont(g, name, barW - EmpireShipPane.s5, 18, 14);
                final int sw = g.getFontMetrics().stringWidth(name);
                final int x2 = barX + (barW - sw) / 2;
                g.drawString(name, x2, barY + EmpireShipPane.s16);
            }
            if (this.hoverBox == this.shipNameBox) {
                final Stroke prev = g.getStroke();
                g.setColor(SystemPanel.yellowText);
                g.setStroke(EmpireShipPane.stroke2);
                g.draw(this.shipNameBox);
                g.setStroke(prev);
            }
        }
        
        private void drawRallyPointButton(final Graphics2D g, final int x, final int y, final int w, final int h) {
            final StarSystem sys = EmpireSystemPanel.this.parentSpritePanel.systemViewToDisplay();
            if (sys == null) {
                return;
            }
            this.rallyPointBox.setBounds(x, y, w, h);
            final GradientPaint pt = new GradientPaint((float)x, (float)y, this.gray175C, (float)(x + w), (float)y, this.gray115C);
            final Paint prevPaint = g.getPaint();
            g.setPaint(pt);
            g.fillRoundRect(x, y, w, h, EmpireShipPane.s10, EmpireShipPane.s10);
            g.setPaint(prevPaint);
            final Stroke prevStroke = g.getStroke();
            if (this.hoverBox == this.rallyPointBox && this.player().canRallyFleetsFrom(sys)) {
                g.setColor(SystemPanel.yellowText);
            }
            else {
                g.setColor(this.gray175C);
            }
            g.setStroke(EmpireShipPane.stroke2);
            g.drawRoundRect(x, y, w, h, EmpireShipPane.s15, EmpireShipPane.s15);
            g.setStroke(prevStroke);
            final String s = this.text("MAIN_COLONY_RELOCATE_LABEL");
            this.scaledFont(g, s, w - EmpireShipPane.s10, 17, 15);
            if (!this.player().canRallyFleetsFrom(sys)) {
                g.setColor(this.gray90C);
            }
            else if (this.hoverBox == this.rallyPointBox) {
                g.setColor(SystemPanel.yellowText);
            }
            else {
                g.setColor(this.gray20C);
            }
            final int sw = g.getFontMetrics().stringWidth(s);
            final int x2 = x + (w - sw) / 2;
            g.drawString(s, x2, y + h - EmpireShipPane.s7);
        }
        
        private void drawTransportButton(final Graphics2D g, final int x, final int y, final int w, final int h) {
            final StarSystem sys = EmpireSystemPanel.this.parentSpritePanel.systemViewToDisplay();
            if (sys == null) {
                return;
            }
            this.transportBox.setBounds(x, y, w, h);
            g.setColor(MainUI.shadeBorderC());
            g.fillRect(x, y, w, h);
            g.setColor(this.darkShadingC);
            g.fillRect(x + EmpireShipPane.s2, y + EmpireShipPane.s5, w - EmpireShipPane.s2, h - EmpireShipPane.s5);
            g.setColor(this.gray190C);
            g.fillRect(x + EmpireShipPane.s2, y + EmpireShipPane.s5, w - EmpireShipPane.s3, h - EmpireShipPane.s7);
            g.setColor(this.buttonC);
            g.fillRect(x + EmpireShipPane.s3, y + EmpireShipPane.s6, w - EmpireShipPane.s5, h - EmpireShipPane.s9);
            if (!this.player().canSendTransportsFrom(sys)) {
                g.setColor(this.gray70C);
            }
            else if (this.hoverBox == this.transportBox) {
                g.setColor(SystemPanel.yellowText);
            }
            else {
                g.setColor(this.textColor);
            }
            g.setFont(this.narrowFont(20));
            final String s = this.text("MAIN_COLONY_TRANSPORTS_LABEL");
            g.drawString(s, x + EmpireShipPane.s10, y + EmpireShipPane.s25);
            if (this.hoverBox == this.transportBox && this.player().canSendTransportsFrom(sys)) {
                final Stroke prevStroke = g.getStroke();
                g.setStroke(EmpireShipPane.stroke2);
                g.setColor(SystemPanel.yellowText);
                g.drawRect(x + EmpireShipPane.s2, y + EmpireShipPane.s5, w - EmpireShipPane.s2, h - EmpireShipPane.s7);
                g.setStroke(prevStroke);
            }
        }
        
        private Image initializedBackgroundImage(final int w, final int h) {
            if (this.starBackground == null || this.starBackground.getWidth() != w || this.starBackground.getHeight() != h) {
                this.starBackground = new BufferedImage(w, h, 2);
                final Graphics g = this.starBackground.getGraphics();
                this.drawBackgroundStars(g, w, h);
                g.dispose();
            }
            return this.starBackground;
        }
        
        private boolean rallyPointEnabled() {
            return this.player().canRallyFleetsFrom(EmpireSystemPanel.this.parentSpritePanel.systemViewToDisplay());
        }
        
        private boolean transportEnabled() {
            return this.player().canSendTransportsFrom(EmpireSystemPanel.this.parentSpritePanel.systemViewToDisplay());
        }
        
        @Override
        public void mouseClicked(final MouseEvent arg0) {
        }
        
        @Override
        public void mouseEntered(final MouseEvent arg0) {
        }
        
        @Override
        public void mouseExited(final MouseEvent arg0) {
            if (this.hoverBox != null) {
                this.hoverBox = null;
                this.repaint();
            }
        }
        
        @Override
        public void mousePressed(final MouseEvent arg0) {
        }
        
        @Override
        public void mouseReleased(final MouseEvent e) {
            if (e.getButton() > 3) {
                return;
            }
            final int x = e.getX();
            final int y = e.getY();
            if (this.shipDesignBox.contains(x, y)) {
                EmpireSystemPanel.this.nextShipDesign();
                this.parent.repaint();
            }
            else if (this.shipNameBox.contains(x, y)) {
                EmpireSystemPanel.this.nextShipDesign();
                this.parent.repaint();
            }
            else if (this.nextDesign.contains(x, y)) {
                EmpireSystemPanel.this.nextShipDesign();
                this.parent.repaint();
            }
            else if (this.prevDesign.contains(x, y)) {
                EmpireSystemPanel.this.prevShipDesign();
                this.parent.repaint();
            }
            else if (this.rallyPointBox.contains(x, y)) {
                if (this.rallyPointEnabled()) {
                    final StarSystem sys = EmpireSystemPanel.this.parentSpritePanel.systemViewToDisplay();
                    if (sys != null) {
                        EmpireSystemPanel.this.parentSpritePanel.parent.clickedSprite(sys.rallySprite());
                    }
                    EmpireSystemPanel.this.parentSpritePanel.repaint();
                }
            }
            else if (this.transportBox.contains(x, y) && this.transportEnabled()) {
                final StarSystem sys = EmpireSystemPanel.this.parentSpritePanel.systemViewToDisplay();
                if (sys != null) {
                    EmpireSystemPanel.this.parentSpritePanel.parent.clickedSprite(sys.transportSprite());
                }
                EmpireSystemPanel.this.parentSpritePanel.repaint();
            }
        }
        
        @Override
        public void mouseDragged(final MouseEvent arg0) {
        }
        
        @Override
        public void mouseMoved(final MouseEvent e) {
            final int x = e.getX();
            final int y = e.getY();
            final Shape prevHover = this.hoverBox;
            this.hoverBox = null;
            if (this.shipDesignBox.contains(x, y)) {
                this.hoverBox = this.shipDesignBox;
            }
            else if (this.shipNameBox.contains(x, y)) {
                this.hoverBox = this.shipNameBox;
            }
            else if (this.nextDesign.contains(x, y)) {
                this.hoverBox = this.nextDesign;
            }
            else if (this.prevDesign.contains(x, y)) {
                this.hoverBox = this.prevDesign;
            }
            else if (this.rallyPointBox.contains(x, y)) {
                this.hoverBox = this.rallyPointBox;
            }
            else if (this.transportBox.contains(x, y)) {
                this.hoverBox = this.transportBox;
            }
            if (prevHover != this.hoverBox) {
                this.repaint();
            }
        }
        
        @Override
        public void mouseWheelMoved(final MouseWheelEvent e) {
            if (e.getWheelRotation() < 0) {
                EmpireSystemPanel.this.nextShipDesign();
            }
            else {
                EmpireSystemPanel.this.prevShipDesign();
            }
            this.parent.repaint();
        }
    }
}
