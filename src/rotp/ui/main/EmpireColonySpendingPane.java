// 
// Decompiled by Procyon v0.5.36
// 

package rotp.ui.main;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseEvent;
import java.awt.Stroke;
import rotp.model.galaxy.StarSystem;
import rotp.model.colony.Colony;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Shape;
import java.awt.Rectangle;
import java.awt.Polygon;
import java.awt.event.MouseWheelListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseListener;
import java.awt.event.KeyEvent;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.GridLayout;
import rotp.ui.SystemViewer;
import java.awt.Color;
import rotp.ui.BasePanel;

public class EmpireColonySpendingPane extends BasePanel
{
    private static final long serialVersionUID = 732858522960266030L;
    static final Color sliderHighlightColor;
    static final Color sliderBoxEnabled;
    static final Color sliderBoxDisabled;
    static final Color sliderErrEnabled;
    static final Color sliderErrDisabled;
    static final Color sliderBackEnabled;
    static final Color sliderBackDisabled;
    static final Color sliderTextEnabled;
    static final Color sliderTextDisabled;
    Color borderHi;
    Color borderLo;
    Color textC;
    Color backC;
    EmpireSliderPane shipSlider;
    EmpireSliderPane defSlider;
    EmpireSliderPane indSlider;
    EmpireSliderPane ecoSlider;
    EmpireSliderPane researchSlider;
    private final SystemViewer parent;
    
    public EmpireColonySpendingPane(final SystemViewer p, final Color c0, final Color text, final Color hi, final Color lo) {
        this.parent = p;
        this.textC = text;
        this.backC = c0;
        this.borderHi = hi;
        this.borderLo = lo;
        this.init();
    }
    
    @Override
    public String textureName() {
        return this.parent.subPanelTextureName();
    }
    
    private void init() {
        this.shipSlider = new EmpireSliderPane(this, 0);
        this.defSlider = new EmpireSliderPane(this, 1);
        this.indSlider = new EmpireSliderPane(this, 2);
        this.ecoSlider = new EmpireSliderPane(this, 3);
        this.researchSlider = new EmpireSliderPane(this, 4);
        final GridLayout layout = new GridLayout(6, 0);
        layout.setVgap(EmpireColonySpendingPane.s1);
        this.setOpaque(true);
        this.setBackground(this.backC);
        this.setLayout(layout);
        this.add(new EmpireSliderPane(this, -1));
        this.add(this.shipSlider);
        this.add(this.defSlider);
        this.add(this.indSlider);
        this.add(this.ecoSlider);
        this.add(this.researchSlider);
    }
    
    @Override
    public void keyPressed(final KeyEvent e) {
        final int k = e.getKeyCode();
        final int mods = e.getModifiersEx();
        Label_0344: {
            switch (k) {
                case KeyEvent.VK_Q:
                {
                    toggleGovernor();
                    break;
                }
                case 49: {
                    switch (mods) {
                        case 0: {
                            this.shipSlider.increment(true);
                            break;
                        }
                        case 1: {
                            this.shipSlider.decrement(true);
                            break;
                        }
                        case 2: {
                            this.shipSlider.toggleLock();
                            break;
                        }
                    }
                    return;
                }
                case 50: {
                    switch (mods) {
                        case 0: {
                            this.defSlider.increment(true);
                            break;
                        }
                        case 1: {
                            this.defSlider.decrement(true);
                            break;
                        }
                        case 2: {
                            this.defSlider.toggleLock();
                            break;
                        }
                    }
                    return;
                }
                case 51: {
                    switch (mods) {
                        case 0: {
                            this.indSlider.increment(true);
                            break;
                        }
                        case 1: {
                            this.indSlider.decrement(true);
                            break;
                        }
                        case 2: {
                            this.indSlider.toggleLock();
                            break;
                        }
                    }
                    return;
                }
                case 52: {
                    switch (mods) {
                        case 0: {
                            this.ecoSlider.increment(true);
                            break;
                        }
                        case 1: {
                            this.ecoSlider.decrement(true);
                            break;
                        }
                        case 2: {
                            this.ecoSlider.toggleLock();
                            break;
                        }
                    }
                    return;
                }
                case 53: {
                    switch (mods) {
                        case 0: {
                            this.researchSlider.increment(true);
                            break Label_0344;
                        }
                        case 1: {
                            this.researchSlider.decrement(true);
                            break Label_0344;
                        }
                        case 2: {
                            this.researchSlider.toggleLock();
                            break Label_0344;
                        }
                    }
                    break;
                }
            }
        }
    }
    
    static {
        sliderHighlightColor = new Color(255, 255, 255);
        sliderBoxEnabled = new Color(34, 140, 142);
        sliderBoxDisabled = new Color(102, 137, 137);
        sliderErrEnabled = new Color(140, 34, 34);
        sliderErrDisabled = new Color(137, 102, 102);
        sliderBackEnabled = Color.black;
        sliderBackDisabled = new Color(65, 65, 65);
        sliderTextEnabled = Color.black;
        sliderTextDisabled = new Color(65, 65, 65);
    }
    
    class EmpireSliderPane extends BasePanel implements MouseListener, MouseMotionListener, MouseWheelListener
    {
        private static final long serialVersionUID = -6288837442606320710L;
        EmpireColonySpendingPane mgmtPane;
        private final Polygon leftArrow;
        private final Polygon rightArrow;
        private final Rectangle labelBox;
        private final Rectangle sliderBox;
        private Shape hoverBox;
        private final int[] leftButtonX;
        private final int[] leftButtonY;
        private final int[] rightButtonX;
        private final int[] rightButtonY;
        private final int category;
        
        EmpireSliderPane(final EmpireColonySpendingPane ui, final int cat) {
            this.leftArrow = new Polygon();
            this.rightArrow = new Polygon();
            this.labelBox = new Rectangle();
            this.sliderBox = new Rectangle();
            this.leftButtonX = new int[3];
            this.leftButtonY = new int[3];
            this.rightButtonX = new int[3];
            this.rightButtonY = new int[3];
            this.mgmtPane = ui;
            this.category = cat;
            this.init();
        }
        
        private void init() {
            this.setOpaque(false);
            this.addMouseListener(this);
            this.addMouseMotionListener(this);
            this.addMouseWheelListener(this);
        }
        
        @Override
        public void paintComponent(final Graphics g0) {
            final Graphics2D g = (Graphics2D)g0;
            super.paintComponent(g);
            final StarSystem sys = EmpireColonySpendingPane.this.parent.systemViewToDisplay();
            if (sys == null) {
                return;
            }
            final Colony colony = sys.colony();
            if (colony == null) {
                return;
            }
            final int w = this.getWidth();
            if (this.category < 0) {
                Color color;
                if (colony.isGovernor()) {
                    color = Color.green;
                } else {
                    color = MainUI.shadeBorderC();
                }
                g.setFont(this.narrowFont(20));
                final String titleText = this.text("MAIN_COLONY_ALLOCATE_SPENDING");
                final int titleY = this.getHeight() - EmpireSliderPane.s6;
                this.drawShadowedString(g, titleText, 2, EmpireSliderPane.s5, titleY, color, EmpireColonySpendingPane.this.textC);
                return;
            }
            final String text = this.text(Colony.categoryName(this.category));
            Color textC;
            if (this.hoverBox == this.labelBox) {
                textC = SystemPanel.yellowText;
            }
            else if (colony.canAdjust(this.category)) {
                textC = EmpireColonySpendingPane.sliderTextEnabled;
            }
            else {
                textC = EmpireColonySpendingPane.sliderTextDisabled;
            }
            final String labelText = this.text(text);
            g.setColor(textC);
            g.setFont(this.narrowFont(18));
            g.drawString(labelText, EmpireSliderPane.s10, this.getHeight() - EmpireSliderPane.s10);
            this.labelBox.setBounds(EmpireSliderPane.s5, 0, this.leftMargin() - EmpireSliderPane.s15, this.getHeight());
            final int boxL = this.boxLeftX();
            final int boxW = this.boxRightX() - boxL;
            final int boxTopY = this.boxTopY();
            final int boxBottomY = this.boxBottomY();
            final int boxH = boxBottomY - boxTopY;
            final float pct = colony.pct(this.category);
            this.leftButtonX[0] = this.leftMargin();
            this.leftButtonX[1] = this.leftMargin() + this.buttonWidth();
            this.leftButtonX[2] = this.leftMargin() + this.buttonWidth();
            this.leftButtonY[0] = this.buttonMidY();
            this.leftButtonY[1] = this.buttonTopY();
            this.leftButtonY[2] = this.buttonBottomY();
            this.rightButtonX[0] = w - this.rightMargin();
            this.rightButtonX[1] = w - this.rightMargin() - this.buttonWidth();
            this.rightButtonX[2] = w - this.rightMargin() - this.buttonWidth();
            this.rightButtonY[0] = this.buttonMidY();
            this.rightButtonY[1] = this.buttonTopY();
            this.rightButtonY[2] = this.buttonBottomY();
            final Color c1 = colony.canAdjust(this.category) ? EmpireColonySpendingPane.sliderBoxEnabled : EmpireColonySpendingPane.sliderBoxDisabled;
            final Color c1a = colony.canAdjust(this.category) ? EmpireColonySpendingPane.sliderErrEnabled : EmpireColonySpendingPane.sliderErrDisabled;
            final Color c2 = colony.canAdjust(this.category) ? EmpireColonySpendingPane.sliderBackEnabled : EmpireColonySpendingPane.sliderBackDisabled;
            Color c3 = (this.hoverBox == this.leftArrow) ? SystemPanel.yellowText : c2;
            g.setColor(c3);
            g.fillPolygon(this.leftButtonX, this.leftButtonY, 3);
            c3 = ((this.hoverBox == this.rightArrow) ? SystemPanel.yellowText : c2);
            g.setColor(c3);
            g.fillPolygon(this.rightButtonX, this.rightButtonY, 3);
            this.leftArrow.reset();
            this.rightArrow.reset();
            for (int i = 0; i < this.leftButtonX.length; ++i) {
                this.leftArrow.addPoint(this.leftButtonX[i], this.leftButtonY[i]);
                this.rightArrow.addPoint(this.rightButtonX[i], this.rightButtonY[i]);
            }
            this.sliderBox.x = boxL;
            this.sliderBox.y = boxTopY;
            this.sliderBox.width = boxW;
            this.sliderBox.height = boxH;
            g.setColor(c2);
            g.fillRect(boxL + this.boxBorderW(), boxTopY, boxW - 2 * this.boxBorderW(), boxH);
            if (colony.warning(this.category)) {
                g.setColor(c1a);
            }
            else {
                g.setColor(c1);
            }
            if (pct == 1.0f) {
                g.fillRect(boxL + this.boxBorderW(), boxTopY + EmpireSliderPane.s2, boxW - 2 * this.boxBorderW(), boxH - EmpireSliderPane.s3);
            }
            else {
                g.fillRect(boxL + this.boxBorderW(), boxTopY + EmpireSliderPane.s2, (int)(pct * (boxW - 2 * this.boxBorderW())), boxH - EmpireSliderPane.s3);
            }
            if (this.hoverBox == this.sliderBox) {
                if (this.category == 3) {
                    final int popGrowth = colony.ecology().upcomingPopGrowth();
                    if (popGrowth != 0) {
                        g.setFont(this.narrowFont(14));
                        g.setColor(Color.lightGray);
                        final String popStr = this.text("MAIN_COLONY_SPENDING_ECO_GROWTH", popGrowth);
                        final int sw1 = g.getFontMetrics().stringWidth(popStr);
                        final int x1 = (boxW - sw1) / 2;
                        g.drawString(popStr, boxL + x1, boxTopY + boxH - EmpireSliderPane.s4);
                    }
                }
                g.setColor(SystemPanel.yellowText);
                final Stroke prev = g.getStroke();
                g.setStroke(EmpireSliderPane.stroke2);
                g.drawRect(boxL + EmpireSliderPane.s3, boxTopY + EmpireSliderPane.s1, boxW - EmpireSliderPane.s6, boxH - EmpireSliderPane.s2);
                g.setStroke(prev);
            }
            final String resultText = this.text(colony.category(this.category).upcomingResult());
            g.setColor(Color.black);
            this.scaledFont(g, resultText, this.rightMargin() - EmpireSliderPane.s10, 18, 14);
            final int sw2 = g.getFontMetrics().stringWidth(resultText);
            g.drawString(resultText, this.getWidth() - sw2 - EmpireSliderPane.s10, this.getHeight() - EmpireSliderPane.s10);
        }
        
        private int leftMargin() {
            return EmpireSliderPane.s58;
        }
        
        private int rightMargin() {
            return EmpireSliderPane.s70;
        }
        
        private int buttonTopY() {
            return EmpireSliderPane.s6;
        }
        
        private int buttonWidth() {
            return EmpireSliderPane.s10;
        }
        
        private int buttonBottomY() {
            return this.getHeight() - EmpireSliderPane.s7;
        }
        
        private int buttonMidY() {
            return (this.buttonTopY() + this.buttonBottomY()) / 2;
        }
        
        private int boxLeftX() {
            return this.leftMargin() + EmpireSliderPane.s10;
        }
        
        private int boxRightX() {
            return this.getWidth() - this.rightMargin() - EmpireSliderPane.s10;
        }
        
        private int boxTopY() {
            return EmpireSliderPane.s6;
        }
        
        private int boxBottomY() {
            return this.getHeight() - EmpireSliderPane.s6;
        }
        
        private int boxBorderW() {
            return EmpireSliderPane.s3;
        }
        
        public void decrement(final boolean click) {
            final StarSystem sys = EmpireColonySpendingPane.this.parent.systemViewToDisplay();
            if (sys == null) {
                return;
            }
            final Colony colony = sys.colony();
            if (colony == null) {
                return;
            }
            if (colony.increment(this.category, -1)) {
                if (click) {
                    this.softClick();
                }
                EmpireColonySpendingPane.this.parent.repaint();
            }
            else if (click) {
                this.misClick();
            }
        }
        
        public void increment(final boolean click) {
            final StarSystem sys = EmpireColonySpendingPane.this.parent.systemViewToDisplay();
            if (sys == null) {
                return;
            }
            final Colony colony = sys.colony();
            if (colony == null) {
                return;
            }
            if (colony.increment(this.category, 1)) {
                if (click) {
                    this.softClick();
                }
                EmpireColonySpendingPane.this.parent.repaint();
            }
            else if (click) {
                this.misClick();
            }
        }
        
        public void toggleLock() {
            this.softClick();
            final StarSystem sys = EmpireColonySpendingPane.this.parent.systemViewToDisplay();
            if (sys == null) {
                return;
            }
            final Colony colony = sys.colony();
            if (colony == null) {
                return;
            }
            colony.toggleLock(this.category);
            this.repaint();
        }
        
        @Override
        public void mouseClicked(final MouseEvent arg0) {
        }
        
        @Override
        public void mouseEntered(final MouseEvent arg0) {
        }
        
        @Override
        public void mouseExited(final MouseEvent arg0) {
            if (this.hoverBox != null) {
                this.hoverBox = null;
                this.repaint();
            }
        }
        
        @Override
        public void mousePressed(final MouseEvent ev) {
        }
        
        @Override
        public void mouseReleased(final MouseEvent e) {
            if (e.getButton() > 3) {
                return;
            }
            final int x = e.getX();
            final int y = e.getY();
            if (this.labelBox.contains(x, y)) {
                this.toggleLock();
            }
            else if (this.leftArrow.contains(x, y)) {
                this.decrement(true);
            }
            else if (this.rightArrow.contains(x, y)) {
                this.increment(true);
            }
            else {
                if (this.category < 0) {
                    toggleGovernor();
                }
                final float pct = this.pctBoxSelected(x, y);
                if (pct >= 0.0f) {
                    final Colony colony = EmpireColonySpendingPane.this.parent.systemViewToDisplay().colony();
                    if (!colony.canAdjust(this.category)) {
                        this.misClick();
                    }
                    else {
                        this.softClick();
                        colony.forcePct(this.category, pct);
                        EmpireColonySpendingPane.this.parent.repaint();
                    }
                }
            }
        }
        
        @Override
        public void mouseDragged(final MouseEvent arg0) {
        }
        
        @Override
        public void mouseMoved(final MouseEvent e) {
            final int x = e.getX();
            final int y = e.getY();
            Shape newHover = null;
            if (this.labelBox.contains(x, y)) {
                newHover = this.labelBox;
            }
            else if (this.sliderBox.contains(x, y)) {
                newHover = this.sliderBox;
            }
            else if (this.leftArrow.contains(x, y)) {
                newHover = this.leftArrow;
            }
            else if (this.rightArrow.contains(x, y)) {
                newHover = this.rightArrow;
            }
            if (newHover != this.hoverBox) {
                this.hoverBox = newHover;
                this.repaint();
            }
        }
        
        @Override
        public void mouseWheelMoved(final MouseWheelEvent e) {
            final int rot = e.getWheelRotation();
            if (this.hoverBox == this.sliderBox) {
                if (rot > 0) {
                    this.decrement(false);
                }
                else if (rot < 0) {
                    this.increment(false);
                }
            }
        }
        
        public float pctBoxSelected(final int x, final int y) {
            final int bw = this.boxBorderW();
            final int minX = this.sliderBox.x + bw;
            final int maxX = this.sliderBox.x + this.sliderBox.width - bw;
            if (x < minX || x > maxX || y < this.boxTopY() + bw || y > this.boxBottomY() - bw) {
                return -1.0f;
            }
            final float num = (float)(x - minX);
            final float den = (float)(maxX - minX);
            return num / den;
        }
    }
    private void toggleGovernor() {
        if (parent.systemViewToDisplay() != null && parent.systemViewToDisplay().colony() != null) {
            Colony colony = parent.systemViewToDisplay().colony();
            colony.setGovernor(!colony.isGovernor());
            if (colony.isGovernor()) {
                colony.govern();
            }
            parent.repaint();
        }
    }
}
