// 
// Decompiled by Procyon v0.5.36
// 

package rotp.model.empires;

import java.util.HashSet;
import java.util.Set;
import rotp.ui.notifications.GNNGenocideNotice;
import rotp.model.incidents.GenocideIncident;
import rotp.ui.notifications.PlunderShipTechNotification;
import rotp.ui.notifications.PlunderTechNotification;
import rotp.ui.notifications.DiscoverTechNotification;
import java.util.Collections;
import rotp.model.tech.Tech;
import rotp.model.colony.ColonyShipyard;
import rotp.model.ships.Design;
import rotp.model.galaxy.Location;
import rotp.ui.notifications.GNNNotification;
import rotp.ui.NoticeMessage;
import rotp.model.galaxy.Transport;
import rotp.model.planet.PlanetType;
import rotp.model.planet.Planet;
import rotp.model.galaxy.IMappedObject;
import java.util.Collection;
import rotp.model.ships.ShipDesign;
import rotp.util.ImageColorizer;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.ImageObserver;
import rotp.model.ships.ShipLibrary;
import java.util.Iterator;
import java.util.ArrayList;
import rotp.model.galaxy.Galaxy;
import java.util.Comparator;
import java.awt.image.BufferedImage;
import java.awt.Color;
import javax.swing.ImageIcon;
import rotp.model.galaxy.ShipFleet;
import rotp.model.colony.Colony;
import rotp.model.galaxy.StarSystem;
import rotp.model.galaxy.Ship;
import java.util.List;
import rotp.model.ships.ShipDesignLab;
import rotp.model.tech.TechTree;
import rotp.model.ai.AI;
import java.io.Serializable;
import rotp.util.Base;

public final class Empire implements Base, Serializable
{
    private static final long serialVersionUID = 3891433873487660434L;
    private static final float SHIP_MAINTENANCE_PCT = 0.02f;
    private static final int MAX_SECURITY_TICKS = 10;
    private static final float MAX_SECURITY_PCT = 0.2f;
    public static final int PLAYER_ID = 0;
    public static final int NULL_ID = -1;
    public static final int ABSTAIN_ID = -2;
    public final int id;
    private final AI ai;
    private Leader leader;
    private final String raceKey;
    private final int raceNameIndex;
    private TechTree tech;
    private final ShipDesignLab shipLab;
    private final int homeSysId;
    public final SystemInfo sv;
    private final EmpireView[] empireViews;
    private final List<Ship> visibleShips;
    private final List<StarSystem> shipBuildingSystems;
    private final List<StarSystem> colonizedSystems;
    private boolean extinct;
    private boolean galacticAlliance;
    private int lastCouncilVoteEmpId;
    private Colony.Orders priorityOrders;
    private int bannerColor;
    private final List<ShipFleet> fleets;
    private final List<StarSystem> newSystems;
    private final EmpireStatus status;
    private float minX;
    private float maxX;
    private float minY;
    private float maxY;
    private float planetScanningRange;
    private float shipScanningRange;
    private boolean knowShipETA;
    private boolean scanPlanets;
    private boolean recalcDistances;
    private float combatTransportPct;
    private int securityAllocation;
    private int empireTaxLevel;
    private float totalReserve;
    private float tradePiracyRate;
    private Empire lastAttacker;
    private transient Race race;
    private transient ImageIcon transportIcon;
    private transient Color ownershipColor;
    private transient Color selectionColor;
    private transient Color reachColor;
    private transient Color shipBorderColor;
    private transient Color scoutBorderColor;
    private transient Color empireRangeColor;
    private transient BufferedImage fleetImg;
    private transient int fleetImageH;
    private transient int fleetImageW;
    public static Comparator<Empire> TOTAL_POPULATION;
    public static Comparator<Empire> TOTAL_PRODUCTION;
    public static Comparator<Empire> AVG_TECH_LEVEL;
    public static Comparator<Empire> TOTAL_FLEET_SIZE;
    
    public static Empire thePlayer() {
        return Galaxy.current().player();
    }
    
    public AI ai() {
        return this.ai;
    }
    
    public Leader leader() {
        return this.leader;
    }
    
    public ShipDesignLab shipLab() {
        return this.shipLab;
    }
    
    public EmpireStatus status() {
        return this.status;
    }
    
    public int homeSysId() {
        return this.homeSysId;
    }
    
    public String unparsedRaceName() {
        return this.race().nameVariant(this.raceNameIndex);
    }
    
    public String raceName() {
        return this.raceName(0);
    }
    
    public String raceName(final int i) {
        final List<String> names = this.substrings(this.unparsedRaceName(), '|');
        if (i >= names.size() || names.get(i).isEmpty()) {
            return names.get(0);
        }
        return names.get(i);
    }
    
    public List<StarSystem> shipBuildingSystems() {
        return this.shipBuildingSystems;
    }
    
    public boolean inGalacticAlliance() {
        return this.galacticAlliance;
    }
    
    public void joinGalacticAlliance() {
        this.galacticAlliance = true;
    }
    
    public float planetScanningRange() {
        return this.planetScanningRange;
    }
    
    public void planetScanningRange(final float d) {
        this.planetScanningRange = d;
    }
    
    public float shipScanningRange() {
        return this.shipScanningRange;
    }
    
    public void shipScanningRange(final float d) {
        this.shipScanningRange = d;
    }
    
    public float combatTransportPct() {
        return this.combatTransportPct;
    }
    
    public void combatTransportPct(final float d) {
        this.combatTransportPct = d;
    }
    
    public float tradePiracyRate() {
        return this.tradePiracyRate;
    }
    
    public void tradePiracyRate(final float f) {
        this.tradePiracyRate = f;
    }
    
    public boolean extinct() {
        return this.extinct;
    }
    
    public TechTree tech() {
        return this.tech;
    }
    
    public float totalReserve() {
        return this.totalReserve;
    }
    
    public Empire lastAttacker() {
        return this.lastAttacker;
    }
    
    public void lastAttacker(final Empire e) {
        this.lastAttacker = e;
    }
    
    public List<ShipFleet> fleets() {
        return this.fleets;
    }
    
    public List<ShipFleet> assignableFleets() {
        if (this.tech().hyperspaceCommunications()) {
            return this.fleets();
        }
        final List<ShipFleet> assignableFleets = new ArrayList<ShipFleet>();
        for (final ShipFleet fl : this.fleets) {
            if (fl.inOrbit()) {
                assignableFleets.add(fl);
            }
        }
        return assignableFleets;
    }
    
    public List<Ship> visibleShips() {
        return this.visibleShips;
    }
    
    public EmpireView[] empireViews() {
        return this.empireViews;
    }
    
    public List<StarSystem> newSystems() {
        return this.newSystems;
    }
    
    public int lastCouncilVoteEmpId() {
        return this.lastCouncilVoteEmpId;
    }
    
    public void lastCouncilVoteEmpId(final int e) {
        this.lastCouncilVoteEmpId = e;
    }
    
    public void knowShipETA(final boolean b) {
        this.knowShipETA = (this.knowShipETA || b);
    }
    
    public void scanPlanets(final boolean b) {
        this.scanPlanets = (this.scanPlanets || b);
    }
    
    public void setRecalcDistances() {
        this.recalcDistances = true;
    }
    
    public Colony.Orders priorityOrders() {
        return this.priorityOrders;
    }
    
    public void priorityOrders(final Colony.Orders o) {
        this.priorityOrders = o;
    }
    
    public int colorId() {
        return this.bannerColor;
    }
    
    public void colorId(final int i) {
        this.bannerColor = i;
    }
    
    public float minX() {
        return this.minX;
    }
    
    public float maxX() {
        return this.maxX;
    }
    
    public float minY() {
        return this.minY;
    }
    
    public float maxY() {
        return this.maxY;
    }
    
    public Race race() {
        if (this.race == null) {
            this.race = Race.keyed(this.raceKey);
        }
        return this.race;
    }
    
    public ImageIcon fleetIcon() {
        return ShipLibrary.current().fleetIcon(this.shipColorId());
    }
    
    public ImageIcon transportIcon() {
        if (this.transportIcon == null) {
            this.transportIcon = ShipLibrary.current().transportIcon(this.shipColorId());
        }
        return this.transportIcon;
    }
    
    public Color ownershipColor() {
        if (this.ownershipColor == null) {
            final Color c = this.color();
            this.ownershipColor = this.newColor(c.getRed(), c.getGreen(), c.getBlue(), 80);
        }
        return this.ownershipColor;
    }
    
    public Color selectionColor() {
        if (this.selectionColor == null) {
            final Color c = this.color();
            this.selectionColor = this.newColor(c.getRed(), c.getGreen(), c.getBlue(), 160);
        }
        return this.selectionColor;
    }
    
    public Color shipBorderColor() {
        if (this.shipBorderColor == null) {
            final Color c = this.color();
            final int cR = c.getRed();
            final int cG = c.getGreen();
            final int cB = c.getBlue();
            this.shipBorderColor = this.newColor(cR * 5 / 8, cG * 5 / 8, cB * 5 / 8);
        }
        return this.shipBorderColor;
    }
    
    public Color scoutBorderColor() {
        if (this.scoutBorderColor == null) {
            final Color c = this.color();
            final int cR = c.getRed();
            final int cG = c.getGreen();
            final int cB = c.getBlue();
            this.scoutBorderColor = this.newColor(cR * 3 / 8, cG * 3 / 8, cB * 3 / 8);
        }
        return this.scoutBorderColor;
    }
    
    public Color empireRangeColor() {
        if (this.empireRangeColor == null) {
            final Color c = this.color();
            final int cR = c.getRed();
            final int cG = c.getGreen();
            final int cB = c.getBlue();
            this.empireRangeColor = this.newColor(cR * 3 / 12, cG * 3 / 12, cB * 3 / 12);
        }
        return this.empireRangeColor;
    }
    
    public Color reachColor() {
        if (this.reachColor == null) {
            final Color c = this.color();
            this.reachColor = this.newColor(c.getRed(), c.getGreen(), c.getBlue(), 48);
        }
        return this.reachColor;
    }
    
    public BufferedImage fleetImage() {
        if (this.fleetImg == null) {
            final Image img = this.fleetIcon().getImage();
            final int w = img.getWidth(null);
            final int h = img.getHeight(null);
            final int sW = this.scaled(w);
            final int sH = this.scaled(h);
            this.fleetImg = new BufferedImage(sW, sH, 2);
            final Graphics2D bGr = this.fleetImg.createGraphics();
            bGr.drawImage(img, 0, 0, sW, sH, 0, 0, w, h, null);
            bGr.dispose();
        }
        return this.fleetImg;
    }
    
    public int fleetImageH() {
        if (this.fleetImageH < 1) {
            this.fleetImageH = this.max(1, this.fleetIcon().getImage().getHeight(null));
        }
        return this.fleetImageH;
    }
    
    public int fleetImageW() {
        if (this.fleetImageW < 1) {
            this.fleetImageW = this.max(1, this.fleetIcon().getImage().getWidth(null));
        }
        return this.fleetImageW;
    }
    
    public Empire(final Galaxy g, final int empId, final String rk, final StarSystem s, final Integer cId, final String name) {
        this.tech = new TechTree();
        this.visibleShips = new ArrayList<Ship>();
        this.shipBuildingSystems = new ArrayList<StarSystem>();
        this.colonizedSystems = new ArrayList<StarSystem>();
        this.extinct = false;
        this.galacticAlliance = false;
        this.lastCouncilVoteEmpId = -1;
        this.priorityOrders = Colony.Orders.NONE;
        this.fleets = new ArrayList<ShipFleet>();
        this.newSystems = new ArrayList<StarSystem>();
        this.planetScanningRange = 0.0f;
        this.shipScanningRange = 0.0f;
        this.knowShipETA = false;
        this.scanPlanets = false;
        this.recalcDistances = true;
        this.combatTransportPct = 0.0f;
        this.securityAllocation = 0;
        this.empireTaxLevel = 0;
        this.totalReserve = 0.0f;
        this.tradePiracyRate = 0.0f;
        this.log("creating empire for ", rk);
        this.id = empId;
        this.raceKey = rk;
        this.homeSysId = s.id;
        this.empireViews = new EmpireView[this.options().selectedNumberOpponents() + 1];
        this.status = new EmpireStatus(this);
        this.sv = new SystemInfo(this);
        this.ai = new AI(this);
        if (this.options().selectedPlayerRace().equals(rk)) {
            g.player(this);
        }
        this.colorId(cId);
        final String raceName = this.race().nextAvailableName();
        this.raceNameIndex = this.race().nameIndex(raceName);
        final String leaderName = (name == null) ? this.race.nextAvailableLeader() : name;
        this.leader = new Leader(this, leaderName);
        this.shipLab = new ShipDesignLab();
    }
    
    public void setBounds(final float x1, final float x2, final float y1, final float y2) {
        this.minX = x1;
        this.maxX = x2;
        this.minY = y1;
        this.maxY = y2;
    }
    
    public void loadStartingTechs() {
        this.tech.recalc(this);
    }
    
    public void loadStartingShipDesigns() {
        this.shipLab.init(this);
    }
    
    public boolean isPlayer() {
        return this.id == 0;
    }
    
    public boolean isAI() {
        return !this.isPlayer();
    }
    
    public boolean isPlayerControlled() {
        return !this.isAIControlled();
    }
    
    public boolean isAIControlled() {
        return this.isAI() || this.options().isAutoPlay();
    }
    
    public Color color() {
        return ImageColorizer.color(this.bannerColor);
    }
    
    public int shipColorId() {
        return this.colorId();
    }
    
    public String name() {
        return this.race().text("GOVT_EMPIRE", this.raceName());
    }
    
    public int shipCount(final int hullSize) {
        int n = 0;
        for (final ShipDesign d : this.shipLab.designs()) {
            if (d != null && d.size() == hullSize) {
                n += d.shipCount();
            }
        }
        return n;
    }
    
    public float totalShipMaintenanceCost() {
        float cost = 0.0f;
        for (final ShipDesign d : this.shipLab.designs()) {
            if (d != null) {
                cost += d.shipCount() * d.cost() * 0.02f;
            }
        }
        return cost;
    }
    
    @Override
    public String toString() {
        return this.concat("Empire: ", this.raceName());
    }
    
    public String replaceTokens(final String s, final String key) {
        final List<String> tokens = this.varTokens(s, key);
        String s2 = s;
        for (final String token : tokens) {
            final String replString = this.concat("[", key, token, "]");
            if (token.equals("_name")) {
                s2 = s2.replace(replString, this.leader().name());
            }
            else {
                final List<String> values = this.substrings(this.race().text(token), ',');
                final String value = (this.raceNameIndex < values.size()) ? values.get(this.raceNameIndex) : values.get(0);
                s2 = s2.replace(replString, value);
            }
        }
        return s2;
    }
    
    public boolean canSendTransportsFrom(final StarSystem sys) {
        return sys != null && this.sv.empire(sys.id) == this && this.sv.maxTransportsToSend(sys.id) > 0 && this.allColonizedSystems().size() > 1 && !sys.colony().quarantined();
    }
    
    public boolean canSendTransportsTo(final StarSystem sys) {
        return sys != null && this.sv.isScouted(sys.id) && this.sv.isColonized(sys.id) && this.canColonize(sys.planet()) && this.sv.inShipRange(sys.id);
    }
    
    public boolean canRallyFleetsFrom(final StarSystem sys) {
        return sys != null && this.sv.empire(sys.id) == this && this.allColonizedSystems().size() > 1;
    }
    
    public boolean canRallyFleetsTo(final StarSystem sys) {
        return sys != null && this.sv.empire(sys.id) == this && this.allColonizedSystems().size() > 1;
    }
    
    public boolean canRallyFleets() {
        return this.allColonizedSystems().size() > 1;
    }
    
    public boolean canSendTransports() {
        return this.allColonizedSystems().size() > 1;
    }
    
    public int maxTransportsAllowed(final StarSystem sys) {
        if (!this.canSendTransportsTo(sys)) {
            return 0;
        }
        if (sys.empire() == this) {
            return this.sv.currentSize(sys.id) - this.sv.population(sys.id);
        }
        return this.sv.currentSize(sys.id);
    }
    
    public void changeAllExistingRallies(final StarSystem dest) {
        if (this.canRallyFleetsTo(dest)) {
            for (final StarSystem sys : this.allColonizedSystems()) {
                if (this.sv.hasRallyPoint(sys.id)) {
                    this.sv.rallySystem(sys.id, dest);
                }
            }
        }
    }
    
    public void startRallies(final List<StarSystem> fromSystems, final StarSystem dest) {
        if (this.canRallyFleetsTo(dest)) {
            for (final StarSystem sys : fromSystems) {
                this.sv.rallySystem(sys.id, dest);
            }
        }
    }
    
    public void stopRallies(final List<StarSystem> fromSystems) {
        for (final StarSystem sys : fromSystems) {
            this.sv.stopRally(sys.id);
        }
    }
    
    public void cancelTransport(final StarSystem from) {
        from.transportSprite().clear();
    }
    
    public void deployTransport(final StarSystem from) {
        from.transportSprite().accept();
    }
    
    public void deployTransports(final List<StarSystem> fromSystems, final StarSystem dest, final boolean synch) {
        if (synch) {
            float maxTime = 0.0f;
            for (final StarSystem from : fromSystems) {
                maxTime = this.max(maxTime, from.colony().transport().travelTime(dest));
            }
            for (final StarSystem from : fromSystems) {
                from.transportSprite().accept(maxTime);
            }
        }
        else {
            for (final StarSystem from2 : fromSystems) {
                from2.transportSprite().accept();
            }
        }
    }
    
    public void validate() {
        this.validateColonizedSystems();
        this.shipLab().validate();
        for (final StarSystem sys : this.colonizedSystems) {
            sys.colony().validate();
        }
    }
    
    private void validateColonizedSystems() {
        final List<StarSystem> good = new ArrayList<StarSystem>();
        for (final StarSystem sys : this.colonizedSystems) {
            if (sys.isColonized() && sys.empire() == this && !good.contains(sys)) {
                good.add(sys);
            }
        }
        this.colonizedSystems.clear();
        this.colonizedSystems.addAll(good);
    }
    
    public void addValidatedFleet(final ShipFleet fl) {
        this.fleets.add(fl);
    }
    
    public void validateShipCounts() {
        final ShipDesignLab lab = this.shipLab();
        for (final ShipDesign d : lab.designs()) {
            d.resetShipCount();
        }
        for (final ShipFleet fl : this.fleets) {
            for (int i = 0; i < 6; ++i) {
                lab.design(i).addShipCount(fl.num(i));
            }
        }
    }
    
    public void cancelTransports(final List<StarSystem> fromSystems) {
        for (final StarSystem from : fromSystems) {
            from.transportSprite().clear();
        }
    }
    
    public void addColonyOrder(final Colony.Orders order, final float amt) {
        for (final StarSystem sys : this.allColonizedSystems()) {
            sys.colony().addColonyOrder(order, amt);
        }
    }
    
    public void addColonizedSystem(final StarSystem s) {
        if (!this.colonizedSystems.contains(s)) {
            this.colonizedSystems.add(s);
            this.setRecalcDistances();
        }
    }
    
    public void removeColonizedSystem(final StarSystem s) {
        this.colonizedSystems.remove(s);
        this.setRecalcDistances();
        if (this.colonizedSystems.isEmpty()) {
            this.goExtinct();
        }
    }
    
    public Colony colonize(final String name, final StarSystem s) {
        final StarSystem home = this.galaxy().system(this.homeSysId);
        this.newSystems.add(s);
        this.addColonizedSystem(s);
        final Colony c = s.becomeColonized(name, this);
        this.ai().setInitialAllocations(c);
        if (this.isPlayer()) {
            this.galaxy().giveAdvice("MAIN_ADVISOR_TRANSPORT", name, this.str((int)(s.planet().maxSize() - s.colony().population())), home.name());
        }
        return c;
    }
    
    public Colony colonizeHomeworld() {
        final StarSystem home = this.galaxy().system(this.homeSysId);
        this.newSystems.add(home);
        this.colonizedSystems.add(home);
        final Colony c = home.becomeColonized(home.name(), this);
        c.setHomeworldValues();
        this.ai().setInitialAllocations(c);
        this.sv.refreshFullScan(this.homeSysId);
        return c;
    }
    
    public boolean isHomeworld(final StarSystem sys) {
        return sys.id == this.homeSysId;
    }
    
    public boolean isColony(final StarSystem sys) {
        return this.sv.empire(sys.id) == this && this.isEnvironmentHostile(sys);
    }
    
    public boolean isEnvironmentHostile(final StarSystem sys) {
        return !this.race().ignoresPlanetEnvironment() && sys.planet().isEnvironmentHostile();
    }
    
    public boolean isEnvironmentFertile(final StarSystem sys) {
        return sys.planet().isEnvironmentFertile();
    }
    
    public boolean isEnvironmentGaia(final StarSystem sys) {
        return sys.planet().isEnvironmentGaia();
    }
    
    public void setBeginningColonyAllocations() {
        final Colony c = this.galaxy().system(this.homeSysId).colony();
        c.clearSpending();
        this.ai().setInitialAllocations(c);
    }
    
    public boolean colonyCanScan(final StarSystem sys) {
        return this.scanPlanets && this.sv.withinRange(sys.id, this.planetScanningRange());
    }
    
    public boolean fleetCanScan(final StarSystem sys) {
        if (!this.scanPlanets) {
            return false;
        }
        for (final ShipFleet fl : this.fleets) {
            if (this.shipScanningRange() >= fl.distanceTo(sys)) {
                return true;
            }
        }
        return false;
    }
    
    public float shipRange() {
        return this.tech().shipRange();
    }
    
    public float shipExtendedRange() {
        return this.tech().scoutRange();
    }
    
    public float shipReach(final int turns) {
        return this.min(this.shipRange(), turns * this.tech().topSpeed());
    }
    
    public float scoutReach(final int turns) {
        return this.min(this.shipExtendedRange(), turns * this.tech().topSpeed());
    }
    
    public boolean canColonize(final Planet p) {
        return this.canColonize(p.type());
    }
    
    public boolean canLearnToColonize(final Planet p) {
        return this.canLearnToColonize(p.type());
    }
    
    public boolean canColonize(final StarSystem sys) {
        return this.canColonize(sys.planet().type());
    }
    
    public boolean canColonize(final PlanetType pt) {
        return pt != null && !pt.isAsteroids() && (this.race().ignoresPlanetEnvironment() || pt.hostility() <= this.tech().hostilityAllowed());
    }
    
    public boolean canLearnToColonize(final PlanetType pt) {
        return pt != null && !pt.isAsteroids() && (this.race().ignoresPlanetEnvironment() || pt.hostility() <= this.tech().learnableHostilityAllowed());
    }
    
    public boolean knowETA(final Ship sh) {
        return sh.empId() == this.id || this.knowShipETA;
    }
    
    public StarSystem defaultSystem() {
        final StarSystem home = this.galaxy().system(this.homeSysId);
        if (home.empire() == this) {
            return home;
        }
        return this.allColonizedSystems().get(0);
    }
    
    public void addViewFor(final Empire emp) {
        if (emp != null && emp != this) {
            this.empireViews[emp.id] = new EmpireView(this, emp);
        }
    }
    
    public float tradeIncomePerBC() {
        final float empireBC = this.totalPlanetaryProduction();
        final float income = this.netTradeIncome();
        return income / empireBC;
    }
    
    public float shipMaintCostPerBC() {
        final float empireBC = this.totalPlanetaryProduction();
        final float shipMaint = this.totalShipMaintenanceCost();
        return shipMaint / empireBC;
    }
    
    public void preNextTurn() {
        if (this.extinct) {
            return;
        }
        for (final ShipFleet fl : this.fleets) {
            fl.reloadBombs();
        }
    }
    
    public void nextTurn() {
        this.log(this + ": NextTurn");
        this.shipBuildingSystems.clear();
        this.newSystems.clear();
        for (final ShipDesign d : this.shipLab.designs()) {
            if (d != null) {
                d.preNextTurn();
            }
        }
        this.refreshViews();
        final List<StarSystem> allColonies = this.allColonizedSystems();
        final List<Transport> transports = this.transports();
        if (!this.extinct && allColonies.isEmpty() && transports.isEmpty()) {
            this.goExtinct();
            return;
        }
        for (final StarSystem s : allColonies) {
            final Colony col = s.planet().colony();
            this.addReserve(col.production() * this.empireTaxPct());
            col.nextTurn();
        }
    }
    
    public void postNextTurn() {
        this.log(this + ": postNextTurn");
        final float civProd = this.totalPlanetaryProduction();
        for (final EmpireView v : this.empireViews()) {
            if (v != null && v.embassy().contact()) {
                v.nextTurn(civProd);
            }
        }
    }
    
    public void assessTurn() {
        this.log(this + ": AssessTurn");
        if (this.status() != null) {
            this.status().assessTurn();
        }
        for (int i = 0; i < this.sv.count(); ++i) {
            if (this.sv.empire(i) == this && this.sv.isColonized(i)) {
                this.sv.colony(i).assessTurn();
            }
        }
    }
    
    public void makeDiplomaticOffers() {
        for (final EmpireView v : this.empireViews()) {
            if (v != null && v.embassy().contact()) {
                v.makeDiplomaticOffers();
            }
        }
    }
    
    public void completeResearch() {
        this.tech.allocateResearch();
    }
    
    public void acquireTradedTechs() {
        this.tech.acquireTradedTechs();
    }
    
    public void makeNextTurnDecisions() {
        long tm0 = System.currentTimeMillis();
        this.log(this + ": make NextTurnDecisions");
        if (this.recalcDistances) {
            NoticeMessage.setSubstatus(this.text("TURN_RECALC_DISTANCES"));
            this.sv.calculateSystemDistances();
            this.recalcDistances = false;
            final long tm2 = System.currentTimeMillis();
            this.log("recalcDistances: " + (tm2 - tm0) + "ms");
            tm0 = tm2;
        }
        NoticeMessage.setSubstatus(this.text("TURN_REFRESHING"));
        this.refreshViews();
        final long tm3 = System.currentTimeMillis();
        this.log("refreshViews: " + (tm3 - tm0) + "ms");
        NoticeMessage.setSubstatus(this.text("TURN_SCRAP_SHIPS"));
        this.shipLab.nextTurn();
        this.ai().fleetCommanderNextTurn();
        NoticeMessage.setSubstatus(this.text("TURN_DESIGN_SHIPS"));
        this.ai().shipDesignerNextTurn();
        NoticeMessage.setSubstatus(this.text("TURN_COLONY_SPENDING"));
        this.setAllocations();

        // If planets are governed, redo allocations now
        for (int i = 0; i < this.sv.count(); ++i) {
            if (this.sv.empire(i) == this && this.sv.isColonized(i)) {
                this.sv.colony(i).governIfNeeded();
            }
        }

        final long tm4 = System.currentTimeMillis();
        this.log("remainder: " + (tm4 - tm3) + "ms");
    }
    
    public String decode(final String s, final Empire listener) {
        String s2 = this.replaceTokens(s, "my");
        s2 = listener.replaceTokens(s2, "your");
        s2 = s2.replace("[title]", listener.race().title());
        s2 = s2.replace("[fulltitle]", listener.race().fullTitle());
        return s2;
    }
    
    public List<Transport> transports() {
        final Galaxy gal = this.galaxy();
        final List<Transport> transports = new ArrayList<Transport>();
        for (final Ship sh : gal.shipsInTransit()) {
            if (sh instanceof Transport && sh.empId() == this.id) {
                transports.add((Transport)sh);
            }
        }
        return transports;
    }
    
    public int transportsInTransit(final StarSystem s) {
        final Galaxy gal = this.galaxy();
        int transports = 0;
        for (final Ship sh : gal.shipsInTransit()) {
            if (sh instanceof Transport && sh.empId() == this.id) {
                final Transport tr = (Transport)sh;
                if (tr.destination() != s) {
                    continue;
                }
                transports += tr.size();
            }
        }
        return transports;
    }
    
    public int transportsToSystem(final StarSystem s) {
        final Galaxy gal = this.galaxy();
        int pop = 0;
        for (final Transport tr : s.transports()) {
            if (tr.empire() == this) {
                pop += tr.size();
            }
        }
        for (final Ship sh : gal.shipsInTransit()) {
            if (sh instanceof Transport) {
                final Transport tr2 = (Transport)sh;
                if (tr2.empire() != this || tr2.destination() != s) {
                    continue;
                }
                pop += tr2.size();
            }
        }
        return pop;
    }
    
    public float transportSpeed(final IMappedObject fr, final IMappedObject to) {
        final float time = fr.travelTime(fr, to, this.tech().transportSpeed());
        final float dist = fr.distanceTo(to);
        return dist / time;
    }
    
    private void setAllocations() {
        if (this.isAIControlled()) {
            this.ai().sendTransports();
        }
        for (int n = 0; n < this.sv.count(); ++n) {
            if (this.sv.empire(n) == this) {
                this.ai().setColonyAllocations(this.sv.colony(n));
            }
        }
        if (!this.isAIControlled()) {
            return;
        }
        this.ai().allocateReserve();
        for (final EmpireView ev : this.empireViews()) {
            if (ev != null && ev.embassy().contact()) {
                ev.setSuggestedAllocations();
            }
        }
        this.ai().setTechTreeAllocations();
        this.securityAllocation = this.ai().suggestedInternalSecurityLevel();
        this.empireTaxLevel = this.ai().suggestedEmpireTaxLevel();
    }
    
    public void checkForRebellionSpread() {
        if (this.extinct) {
            return;
        }
        final float perRebellionPct = this.min(0.01f, 1.0f / this.galaxy().numStarSystems());
        float rebellionPct = 0.0f;
        final List<StarSystem> allSystems = this.allColonizedSystems();
        for (final StarSystem sys : allSystems) {
            if (sys.colony().inRebellion()) {
                rebellionPct += perRebellionPct;
            }
        }
        if (rebellionPct > 0.0f) {
            for (final StarSystem sys : allSystems) {
                if (!sys.colony().inRebellion() && !sys.colony().isHomeworld() && this.random() < rebellionPct) {
                    sys.colony().spreadRebellion();
                }
            }
        }
    }
    
    public boolean inRevolt() {
        float rebellingPop = 0.0f;
        float loyalPop = 0.0f;
        for (final StarSystem sys : this.allColonizedSystems()) {
            if (sys.colony().inRebellion()) {
                rebellingPop += sys.colony().population();
            }
            else {
                loyalPop += sys.colony().population();
            }
        }
        return rebellingPop > loyalPop;
    }
    
    public void overthrowLeader() {
        if (this.isPlayer()) {
            return;
        }
        this.leader = new Leader(this);
        for (final EmpireView view : this.empireViews()) {
            if (view != null) {
                view.breakAllTreaties();
            }
        }
        for (final StarSystem sys : this.allColonizedSystems()) {
            sys.colony().rebels(0);
        }
        if (this.viewForEmpire(this.player()).embassy().contact()) {
            final String leaderDesc = this.text("LEADER_PERSONALITY_FORMAT", this.leader.personality(), this.leader.objective());
            final String message = this.text("GNN_OVERTHROW", this.name(), leaderDesc);
            GNNNotification.notifyRebellion(message);
        }
    }
    
    public boolean inEconomicRange(final Empire e) {
        for (final StarSystem sys : this.systemsForCiv(e)) {
            if (this.sv.inScoutRange(sys.id)) {
                return true;
            }
        }
        for (final StarSystem sys : e.systemsForCiv(this)) {
            if (this.sv.inScoutRange(sys.id)) {
                return true;
            }
        }
        return false;
    }
    
    public void makeFullContact() {
        for (final EmpireView v : this.empireViews()) {
            if (v != null && !v.embassy().contact()) {
                v.embassy().makeFirstContact();
            }
        }
    }
    
    public float orionCouncilBonus() {
        final Galaxy gal = this.galaxy();
        for (int i = 0; i < this.sv.count(); ++i) {
            if (this.sv.empire(i) == this && gal.system(i).isGuarded()) {
                return 0.2f;
            }
        }
        return 0.0f;
    }
    
    public void addVisibleShip(final Ship sh) {
        if (!this.visibleShips.contains(sh)) {
            this.visibleShips.add(sh);
        }
    }
    
    public void addVisibleShips(final List<? extends Ship> ships) {
        if (ships != null) {
            for (final Ship sh : ships) {
                this.addVisibleShip(sh);
            }
        }
    }
    
    public void addFleet(final ShipFleet fl) {
        if (fl.isEmpty()) {
            return;
        }
        this.fleets.add(fl);
        this.addVisibleShip(fl);
    }
    
    public void removeFleet(final ShipFleet fl) {
        this.fleets.remove(fl);
        this.visibleShips().remove(fl);
    }
    
    public List<ShipFleet> enemyFleets() {
        final List<ShipFleet> list = new ArrayList<ShipFleet>();
        for (final Ship sh : this.visibleShips()) {
            if (sh instanceof ShipFleet) {
                final ShipFleet fl = (ShipFleet)sh;
                if (fl.empId() == this.id || fl.isEmpty()) {
                    continue;
                }
                list.add((ShipFleet)sh);
            }
        }
        return list;
    }
    
    public void startGame() {
        this.refreshViews();
        final StarSystem home = this.galaxy().system(this.homeSysId);
        this.ai().setInitialAllocations(home.colony());
    }
    
    public void refreshViews() {
        final Galaxy gal = this.galaxy();
        for (int i = 0; i < this.sv.count(); ++i) {
            final StarSystem sys = gal.system(i);
            if (sys.empire() == this) {
                this.sv.refreshFullScan(i);
            }
            else if (sys.orbitingFleetForEmpire(this) != null && !sys.orbitingShipsInConflict()) {
                this.sv.refreshFullScan(i);
            }
            else if (this.colonyCanScan(sys)) {
                this.sv.refreshLongRangeScan(i);
            }
            else if (this.fleetCanScan(sys)) {
                this.sv.refreshLongRangeScan(i);
            }
            else if (this.sv.isScouted(i)) {
                this.sv.view(i).clearFleetInfo();
            }
        }
        for (final EmpireView v : this.empireViews()) {
            if (v != null) {
                v.refresh();
            }
        }
        for (int n = 0; n < this.sv.count(); ++n) {
            this.sv.resetSystemData(n);
        }
        this.setVisibleShips();
    }
    
    public void setVisibleShips(final int sysId) {
        this.addVisibleShips(this.sv.orbitingFleets(sysId));
        this.addVisibleShips(this.sv.exitingFleets(sysId));
    }
    
    public void setVisibleShips() {
        final Galaxy gal = this.galaxy();
        this.visibleShips.clear();
        final float scanRange = this.planetScanningRange();
        for (int i = 0; i < this.sv.count(); ++i) {
            if (this.sv.withinRange(i, scanRange) || this.sv.hasFleetForEmpire(i, this)) {
                final StarSystem sys = this.sv.system(i);
                this.addVisibleShips(sys.orbitingFleets());
                this.addVisibleShips(sys.exitingFleets());
            }
        }
        for (final Ship sh : gal.shipsInTransit()) {
            if (sh.empId() == this.id || (sh.visibleTo(this.id) && this.canScanTo(sh))) {
                this.addVisibleShip(sh);
            }
        }
        for (final Ship fl : this.visibleShips) {
            if (fl instanceof ShipFleet) {
                this.detectFleet((ShipFleet)fl);
            }
        }
    }
    
    public boolean canScanTo(final IMappedObject loc) {
        return this.planetsCanScanTo(loc) || this.shipsCanScanTo(loc);
    }
    
    public boolean planetsCanScanTo(final IMappedObject loc) {
        if (this.planetScanningRange() == 0.0f) {
            return false;
        }
        final Galaxy gal = this.galaxy();
        for (int i = 0; i < this.sv.count(); ++i) {
            if (this.sv.empire(i) == this && gal.system(i).distanceTo(loc) <= this.planetScanningRange()) {
                return true;
            }
        }
        return false;
    }
    
    public boolean shipsCanScanTo(final IMappedObject loc) {
        if (this.shipScanningRange == 0.0f) {
            return false;
        }
        for (final ShipFleet fl : this.fleets) {
            if (fl.distanceTo(loc) < this.shipScanningRange) {
                return true;
            }
        }
        return false;
    }
    
    public float estimatedShipFirepower(final Empire emp, final int shipSize, final int shieldLevel) {
        return 0.0f;
    }
    
    public boolean canTransportTo(final Location xyz) {
        final Galaxy gal = this.galaxy();
        for (int i = 0; i < gal.numStarSystems(); ++i) {
            final StarSystem s = gal.system(i);
            if (s.empire() == this && s.distanceTo(xyz) <= this.tech.shipRange()) {
                return true;
            }
        }
        return false;
    }
    
    public boolean canScoutTo(final Location xyz) {
        final Galaxy gal = this.galaxy();
        for (int i = 0; i < gal.numStarSystems(); ++i) {
            final StarSystem s = gal.system(i);
            if (s.empire() == this && s.distanceTo(xyz) <= this.tech.scoutRange()) {
                return true;
            }
        }
        return false;
    }
    
    public float distanceToSystem(final StarSystem sys, final List<StarSystem> froms) {
        float distance = Float.MAX_VALUE;
        for (final StarSystem from : froms) {
            distance = this.min(from.distanceTo(sys), distance);
        }
        return distance;
    }
    
    public float distanceTo(final IMappedObject xyz) {
        float distance = Float.MAX_VALUE;
        final Galaxy gal = this.galaxy();
        final List<Empire> allies = this.allies();
        if (allies.isEmpty()) {
            for (int i = 0; i < gal.numStarSystems(); ++i) {
                final StarSystem s = gal.system(i);
                if (s.empire() == this) {
                    distance = this.min(s.distanceTo(xyz), distance);
                }
                if (distance == 0.0f) {
                    return distance;
                }
            }
        }
        else {
            for (int i = 0; i < gal.numStarSystems(); ++i) {
                final StarSystem s = gal.system(i);
                if (this.alliedWith(s.empire())) {
                    distance = this.min(s.distanceTo(xyz), distance);
                }
                if (distance == 0.0f) {
                    return distance;
                }
            }
        }
        return distance;
    }
    
    public int rangeTo(final StarSystem sys) {
        return (int)Math.ceil(this.sv.distance(sys.id));
    }
    
    public StarSystem mostPopulousSystemForCiv(final Empire c) {
        StarSystem bestSystem = null;
        StarSystem biggestSystem = null;
        float maxPop1 = 0.0f;
        float maxPop2 = 0.0f;
        for (final StarSystem sys : this.systemsForCiv(c)) {
            final float sysPop = (float)this.sv.population(sys.id);
            final Colony col = this.sv.colony(sys.id);
            if (sysPop > maxPop1) {
                maxPop1 = sysPop;
                biggestSystem = sys;
            }
            if (col != null && sysPop > maxPop2 && !col.inRebellion()) {
                maxPop2 = sysPop;
                bestSystem = sys;
            }
        }
        if (bestSystem != null) {
            return bestSystem;
        }
        return biggestSystem;
    }
    
    public boolean isAnyColonyConstructing(final ShipDesign d) {
        for (int n = 0; n < this.sv.count(); ++n) {
            if (this.sv.empire(n) == this && this.sv.colony(n).shipyard().queuedBCForDesign(d) > 0.0f) {
                return true;
            }
        }
        return false;
    }
    
    public List<StarSystem> coloniesConstructing(final ShipDesign d) {
        final Galaxy gal = this.galaxy();
        final List<StarSystem> colonies = new ArrayList<StarSystem>();
        for (int i = 0; i < this.sv.count(); ++i) {
            if (this.sv.empire(i) == this && this.sv.colony(i).shipyard().queuedBCForDesign(d) > 0.0f) {
                colonies.add(gal.system(i));
            }
        }
        return colonies;
    }
    
    public int shipDesignCount(final int designId) {
        int count = 0;
        for (final ShipFleet f : this.fleets) {
            count += f.num(designId);
        }
        return count;
    }
    
    public void swapShipConstruction(final ShipDesign oldDesign) {
        this.swapShipConstruction(oldDesign, null);
    }
    
    public void swapShipConstruction(final ShipDesign oldDesign, final ShipDesign newDesign) {
        for (final StarSystem sys : this.allColonizedSystems()) {
            final ColonyShipyard shipyard = sys.colony().shipyard();
            if (shipyard.design() == oldDesign) {
                if (newDesign != null && newDesign.active()) {
                    shipyard.switchToDesign(newDesign);
                }
                else {
                    shipyard.goToNextDesign();
                }
            }
        }
    }
    
    public EmpireView viewForEmpire(final Empire emp) {
        if (emp != null && emp != this) {
            return this.empireViews[emp.id];
        }
        return null;
    }
    
    public boolean hasContact(final Empire c) {
        final EmpireView v = this.viewForEmpire(c);
        return v != null && v.embassy().contact() && !v.empire().extinct;
    }
    
    public void makeContact(final Empire c) {
        final EmpireView v = this.viewForEmpire(c);
        if (v != null) {
            v.setContact();
        }
    }
    
    public boolean atWar() {
        for (final EmpireView v : this.empireViews()) {
            if (v != null && v.embassy().anyWar()) {
                return true;
            }
        }
        return false;
    }
    
    public float powerLevel(final Empire e) {
        return this.militaryPowerLevel(e) + this.industrialPowerLevel(e);
    }
    
    public float militaryPowerLevel(final Empire e) {
        final TechTree t0 = (e == this) ? this.tech() : this.viewForEmpire(e).spies().tech();
        final float fleet = this.totalFleetSize(e);
        final float techLvl = t0.avgTechLevel();
        return fleet * techLvl;
    }
    
    public float militaryPowerLevel() {
        final float fleet = this.totalArmedFleetSize();
        final float techLvl = this.tech().avgTechLevel();
        return fleet * techLvl;
    }
    
    public float industrialPowerLevel(final Empire e) {
        final TechTree t0 = (e == this) ? this.tech() : this.viewForEmpire(e).spies().tech();
        final float prod = this.totalPlanetaryProduction(e);
        final float techLvl = t0.avgTechLevel();
        return prod * techLvl;
    }
    
    public boolean hasAnyContact() {
        return !this.contactedEmpires().isEmpty();
    }
    
    public List<Empire> contactedEmpires() {
        final List<Empire> r = new ArrayList<Empire>();
        for (final EmpireView v : this.empireViews()) {
            if (v != null && v.embassy().contact() && !v.empire().extinct) {
                r.add(v.empire());
            }
        }
        return r;
    }
    
    public List<EmpireView> contactedCivsThatKnow(final Tech t) {
        final List<EmpireView> r = new ArrayList<EmpireView>();
        for (final EmpireView cv : this.empireViews()) {
            if (cv != null && cv.embassy().contact() && cv.spies().tech().knows(t)) {
                r.add(cv);
            }
        }
        return r;
    }
    
    public int treatyLevelForCiv(final Empire c) {
        final EmpireView v = this.viewForEmpire(c);
        if (v == null) {
            return -1;
        }
        return v.embassy().treatyLevel();
    }
    
    public int numColonies() {
        int count = 0;
        for (int n = 0; n < this.sv.count(); ++n) {
            if (this.sv.empire(n) == this) {
                ++count;
            }
        }
        return count;
    }
    
    public float totalSpyCostPct() {
        float sum = 0.0f;
        for (final EmpireView ev : this.empireViews()) {
            if (ev != null) {
                sum += ev.spies().allocationCostPct();
            }
        }
        return sum;
    }
    
    public int totalActiveSpies() {
        int sum = 0;
        for (final EmpireView ev : this.empireViews()) {
            if (ev != null) {
                sum += ev.spies().numActiveSpies();
            }
        }
        return sum;
    }
    
    public int internalSecurity() {
        return this.securityAllocation;
    }
    
    public void internalSecurity(final int i) {
        this.securityAllocation = this.bounds(0, i, 10);
    }
    
    public float internalSecurityPct() {
        return this.securityAllocation / 10.0f;
    }
    
    public void increaseInternalSecurity() {
        this.internalSecurity(this.securityAllocation + 1);
    }
    
    public void decreaseInternalSecurity() {
        this.internalSecurity(this.securityAllocation - 1);
    }
    
    public void securityAllocation(final float d) {
        final float incr = 0.09090909f;
        float sum = 0.0f;
        for (int i = 0; i < 11; ++i) {
            sum += incr;
            if (d <= sum) {
                this.internalSecurity(i);
                return;
            }
        }
        this.internalSecurity(10);
    }
    
    public float totalInternalSecurityPct() {
        return 0.2f * this.securityAllocation / 10.0f;
    }
    
    public float internalSecurityCostPct() {
        return this.totalInternalSecurityPct() / 2.0f;
    }
    
    public float totalSecurityCostPct() {
        return this.totalSpyCostPct() + this.internalSecurityCostPct();
    }
    
    public float baseSpyCost() {
        return (25.0f + this.tech.computer().techLevel() * 2.0f) * this.race().spyCostMod();
    }
    
    public float troopKillRatio(final StarSystem s) {
        final float killRatio = (50.0f + this.tech.troopCombatAdj(false)) / (50.0f + this.sv.defenderCombatAdj(s.id));
        return killRatio;
    }
    
    public List<EmpireView> contacts() {
        final List<EmpireView> r = new ArrayList<EmpireView>();
        for (final EmpireView v : this.empireViews()) {
            if (v != null && !v.empire().extinct && v.embassy().contact()) {
                r.add(v);
            }
        }
        return r;
    }
    
    public List<EmpireView> commonContacts(final Empire emp2) {
        final List<EmpireView> r = new ArrayList<EmpireView>();
        if (emp2.extinct) {
            return r;
        }
        for (final EmpireView v : this.empireViews()) {
            if (v != null && !v.empire().extinct && v.embassy().contact()) {
                if (v.empire() == emp2) {
                    r.add(v);
                }
                else {
                    final EmpireView v2 = v.empire().viewForEmpire(emp2);
                    if (v2.embassy().contact()) {
                        r.add(v);
                    }
                }
            }
        }
        return r;
    }
    
    public int numEnemies() {
        int n = 0;
        for (final EmpireView v : this.empireViews()) {
            if (v != null && !v.empire().extinct && (v.embassy().anyWar() || v.embassy().onWarFooting())) {
                ++n;
            }
        }
        return n;
    }
    
    public List<Empire> enemies() {
        final List<Empire> r = new ArrayList<Empire>();
        for (final EmpireView v : this.empireViews()) {
            if (v != null && !v.empire().extinct && (v.embassy().anyWar() || v.embassy().onWarFooting())) {
                r.add(v.empire());
            }
        }
        return r;
    }
    
    public List<EmpireView> enemyViews() {
        final List<EmpireView> r = new ArrayList<EmpireView>();
        for (final EmpireView v : this.empireViews()) {
            if (v != null && !v.empire().extinct && (v.embassy().anyWar() || v.embassy().onWarFooting())) {
                r.add(v);
            }
        }
        return r;
    }
    
    public List<EmpireView> hostiles() {
        final List<EmpireView> r = new ArrayList<EmpireView>();
        for (final EmpireView v : this.empireViews()) {
            if (v != null && !v.empire().extinct && !v.embassy().isFriend()) {
                r.add(v);
            }
        }
        return r;
    }
    
    public boolean hasNonEnemiesKnownBy(final Empire e) {
        for (final EmpireView v : this.empireViews()) {
            if (v != null && v.empire() != e && !v.empire().extinct && !v.embassy().anyWar() && e.hasContact(v.empire())) {
                return true;
            }
        }
        return false;
    }
    
    public List<Empire> nonEnemiesKnownBy(final Empire e) {
        final List<Empire> enemies = new ArrayList<Empire>();
        for (final EmpireView v : this.empireViews()) {
            if (v != null && v.empire() != e && !v.empire().extinct && !v.embassy().anyWar() && e.hasContact(v.empire())) {
                enemies.add(v.empire());
            }
        }
        return enemies;
    }
    
    public List<Empire> allies() {
        final List<Empire> r = new ArrayList<Empire>();
        for (final EmpireView v : this.empireViews()) {
            if (v != null && !v.empire().extinct && v.embassy().isAlly()) {
                r.add(v.empire());
            }
        }
        return r;
    }
    
    public boolean hasAlliesKnownBy(final Empire emp1) {
        for (final EmpireView v : this.empireViews()) {
            if (v != null && !v.empire().extinct && v.empire() != emp1 && v.embassy().isAlly() && emp1.hasContact(v.empire())) {
                return true;
            }
        }
        return false;
    }
    
    public List<Empire> alliesKnownBy(final Empire emp1) {
        final List<Empire> allies = new ArrayList<Empire>();
        for (final EmpireView v : this.empireViews()) {
            if (v != null && !v.empire().extinct && v.empire() != emp1 && v.embassy().isAlly() && emp1.hasContact(v.empire())) {
                allies.add(v.empire());
            }
        }
        return allies;
    }
    
    public boolean friendlyWith(final Empire c) {
        if (c == this) {
            return true;
        }
        if (c == null) {
            return false;
        }
        if (c.extinct) {
            return false;
        }
        final EmpireView v = this.viewForEmpire(c);
        return v != null && v.embassy().isFriend();
    }
    
    public boolean pactWith(final Empire c) {
        if (c == this) {
            return true;
        }
        if (c == null) {
            return false;
        }
        if (c.extinct) {
            return false;
        }
        final EmpireView v = this.viewForEmpire(c);
        return v != null && v.embassy().pact();
    }
    
    public boolean alliedWith(final Empire c) {
        if (c == this) {
            return true;
        }
        if (c == null) {
            return false;
        }
        if (c.extinct) {
            return false;
        }
        final EmpireView v = this.viewForEmpire(c);
        return v != null && v.embassy().alliance();
    }
    
    public boolean tradingWith(final Empire c) {
        if (c == this) {
            return true;
        }
        if (c == null) {
            return false;
        }
        if (c.extinct) {
            return false;
        }
        final EmpireView v = this.viewForEmpire(c);
        return v != null && v.trade().active();
    }
    
    public boolean aggressiveWith(final Empire c) {
        if (c == this) {
            return false;
        }
        if (c == null) {
            return false;
        }
        if (c.extinct) {
            return true;
        }
        final EmpireView v = this.viewForEmpire(c);
        return v == null || v.embassy().canAttackWithoutPenalty();
    }
    
    public boolean aggressiveWith(final Empire c, final StarSystem s) {
        if (c == this) {
            return false;
        }
        if (c == null) {
            return false;
        }
        if (c.extinct) {
            return true;
        }
        final EmpireView v = this.viewForEmpire(c);
        return v == null || v.embassy().canAttackWithoutPenalty(s);
    }
    
    public boolean atWarWith(final Empire c) {
        if (c == this) {
            return false;
        }
        if (c == null) {
            return false;
        }
        if (c.extinct) {
            return false;
        }
        final EmpireView v = this.viewForEmpire(c);
        return v != null && v.embassy().anyWar();
    }
    
    public boolean hasTradeWith(final Empire c) {
        if (c == this) {
            return false;
        }
        if (c == null) {
            return false;
        }
        if (c.extinct) {
            return false;
        }
        final EmpireView v = this.viewForEmpire(c);
        return v != null && v.trade().active();
    }
    
    public int contactAge(final Empire c) {
        if (c == this) {
            return 0;
        }
        if (c == null) {
            return 0;
        }
        if (c.extinct) {
            return 0;
        }
        final EmpireView v = this.viewForEmpire(c);
        if (v == null) {
            return 0;
        }
        return v.embassy().contactAge();
    }
    
    public List<StarSystem> systemsNeedingTransports(final int minTransport) {
        final List<StarSystem> systems = new ArrayList<StarSystem>();
        for (final StarSystem sys : this.colonizedSystems) {
            if (sys.colony().inRebellion() || this.sv.popNeeded(sys.id) >= minTransport) {
                systems.add(sys);
            }
        }
        return systems;
    }
    
    public List<StarSystem> systemsInShipRange(final Empire c) {
        final Galaxy gal = this.galaxy();
        final List<StarSystem> systems = new ArrayList<StarSystem>();
        for (int n = 0; n > this.sv.count(); ++n) {
            final StarSystem sys = gal.system(n);
            if (this.sv.inShipRange(sys.id) && (c == null || this.sv.empire(sys.id) == c)) {
                systems.add(sys);
            }
        }
        return systems;
    }
    
    public List<StarSystem> systemsSparingTransports(final int minTransport) {
        final List<StarSystem> systems = new ArrayList<StarSystem>();
        for (final StarSystem sys : this.colonizedSystems) {
            if (!sys.colony().inRebellion() && this.sv.maxPopToGive(sys.id) >= minTransport) {
                systems.add(sys);
            }
        }
        return systems;
    }
    
    public List<StarSystem> allColonizedSystems() {
        return this.colonizedSystems;
    }
    
    public List<StarSystem> allySystems() {
        final Galaxy gal = this.galaxy();
        final List<StarSystem> systems = new ArrayList<StarSystem>();
        for (int i = 0; i < this.sv.count(); ++i) {
            if (this.alliedWith(this.sv.empire(i))) {
                systems.add(gal.system(i));
            }
        }
        return systems;
    }
    
    public StarSystem colonyNearestToSystem(final StarSystem sys) {
        final List<StarSystem> colonies = new ArrayList<StarSystem>(this.allColonizedSystems());
        colonies.remove(sys);
        if (colonies.isEmpty()) {
            return null;
        }
        StarSystem.TARGET_SYSTEM = sys;
        Collections.sort(colonies, StarSystem.DISTANCE_TO_TARGET_SYSTEM);
        return colonies.get(0);
    }
    
    public List<StarSystem> allyColoniesNearestToSystem(final StarSystem sys) {
        final List<StarSystem> colonies = this.allySystems();
        colonies.remove(sys);
        StarSystem.TARGET_SYSTEM = sys;
        Collections.sort(colonies, StarSystem.DISTANCE_TO_TARGET_SYSTEM);
        return colonies;
    }
    
    public StarSystem alliedColonyNearestToSystem(final StarSystem s) {
        final List<StarSystem> colonies = this.allyColoniesNearestToSystem(s);
        return colonies.isEmpty() ? null : colonies.get(0);
    }
    
    public List<StarSystem> systemsForCiv(final Empire emp) {
        final Galaxy gal = this.galaxy();
        final List<StarSystem> systems = new ArrayList<StarSystem>();
        for (int i = 0; i < this.sv.count(); ++i) {
            if (this.sv.empire(i) == emp) {
                systems.add(gal.system(i));
            }
        }
        return systems;
    }
    
    public int numSystemsForCiv(final Empire emp) {
        int num = 0;
        for (int n = 0; n < this.sv.count(); ++n) {
            if (this.sv.empire(n) == emp) {
                ++num;
            }
        }
        return num;
    }
    
    public int numColonizedSystems() {
        return this.colonizedSystems.size();
    }
    
    public List<ShipFleet> fleetsForEmpire(final Empire c) {
        final List<ShipFleet> fleets2 = new ArrayList<ShipFleet>();
        for (final Ship sh : this.visibleShips()) {
            if (sh.empId() == c.id && sh instanceof ShipFleet) {
                fleets2.add((ShipFleet)sh);
            }
        }
        return fleets2;
    }
    
    public boolean anyUnexploredSystems() {
        for (int n = 0; n < this.sv.count(); ++n) {
            if (!this.sv.isScouted(n)) {
                return true;
            }
        }
        return false;
    }
    
    public List<StarSystem> unexploredSystems() {
        final Galaxy gal = this.galaxy();
        final List<StarSystem> systems = new ArrayList<StarSystem>();
        for (int n = 0; n < this.sv.count(); ++n) {
            if (!this.sv.isScouted(n)) {
                systems.add(gal.system(n));
            }
        }
        return systems;
    }
    
    public List<StarSystem> uncolonizedPlanetsInShipRange(final int bestType) {
        final Galaxy gal = this.galaxy();
        final List<StarSystem> systems = new ArrayList<StarSystem>();
        for (int i = 0; i < this.sv.count(); ++i) {
            final StarSystem sys = gal.system(i);
            if (this.sv.isScouted(i) && this.sv.inShipRange(i) && this.canColonize(sys.planet())) {
                systems.add(sys);
            }
        }
        return systems;
    }
    
    public PlanetType minUncolonizedPlanetTypeInShipRange(final boolean checkHabitable) {
        PlanetType minType = PlanetType.keyed("PLANET_TERRAN");
        for (int n = 0; n < this.sv.count(); ++n) {
            if (this.sv.isScouted(n) && this.sv.inShipRange(n) && !this.sv.isColonized(n)) {
                final PlanetType pType = this.sv.planetType(n);
                if ((!checkHabitable || this.canColonize(pType)) && pType.hostility() > minType.hostility()) {
                    minType = pType;
                }
            }
        }
        return minType;
    }
    
    public boolean knowsAllActiveEmpires() {
        for (final Empire e : this.galaxy().activeEmpires()) {
            if (this != e && !this.knowsOf(e)) {
                return false;
            }
        }
        return true;
    }
    
    public boolean hasContacted(final Empire e) {
        final EmpireView ev = this.viewForEmpire(e);
        return ev != null && ev.embassy().contact();
    }
    
    public boolean knowsOf(final Empire e) {
        if (e == this) {
            return true;
        }
        if (this.hasContacted(e)) {
            return true;
        }
        for (final Empire emp : this.contactedEmpires()) {
            if (emp.hasContacted(e)) {
                return true;
            }
        }
        return false;
    }
    
    public List<ShipFleet> fleetsTargetingSystem(final StarSystem target) {
        final List<ShipFleet> fleets1 = new ArrayList<ShipFleet>();
        for (final ShipFleet fl : fleets1) {
            if (fl.inTransit() && fl.transitDest() == target) {
                fleets1.add(fl);
            }
            else {
                if (fl.inTransit() || fl.system() != target) {
                    continue;
                }
                fleets1.add(fl);
            }
        }
        return fleets1;
    }
    
    public void scrapExcessBases(final StarSystem sys, final int max) {
        if (this.sv.empire(sys.id) == this) {
            final Colony col = sys.colony();
            if (col.defense().bases() > max) {
                this.log("civScrapBases  bases:", this.str(col.defense().bases()), " max: ", this.str(max), " cost: ", this.str(this.tech.newMissileBaseCost()));
                this.totalReserve += (col.defense().bases() - max) * this.tech.newMissileBaseCost() / 4.0f;
                col.defense().bases((float)max);
                this.sv.refreshFullScan(sys.id);
            }
        }
    }
    
    public float bestEnemyShieldLevel() {
        float best = 0.0f;
        for (final EmpireView v : this.empireViews()) {
            if (v != null) {
                best = this.max(best, v.spies().tech().maxDeflectorShieldLevel());
            }
        }
        return best;
    }
    
    public float bestEnemyPlanetaryShieldLevel() {
        float best = 0.0f;
        for (final EmpireView v : this.empireViews()) {
            if (v != null) {
                final float shieldLevel = v.spies().tech().maxDeflectorShieldLevel() + v.spies().tech().maxPlanetaryShieldLevel();
                best = this.max(best, shieldLevel);
            }
        }
        return best;
    }
    
    public void addToTreasury(final float amt) {
        this.totalReserve += amt;
    }
    
    public void addReserve(final float amt) {
        this.addToTreasury(amt / 2.0f);
    }
    
    public void stealTech(final String id) {
        this.tech().learnTech(id);
        this.log("Tech: " + this.tech(id).name(), " stolen");
    }
    
    public void learnTech(final String id) {
        final boolean newTech = this.tech().learnTech(id);
        if (newTech && this.isPlayer()) {
            this.log("Tech: ", id, " researched");
            DiscoverTechNotification.create(id);
        }
    }
    
    public void plunderTech(final Tech t, final StarSystem s, final Empire emp) {
        final boolean newTech = this.tech().learnTech(t.id);
        if (newTech && this.isPlayer()) {
            this.log("Tech: ", t.name(), " plundered from: ", s.name());
            PlunderTechNotification.create(t.id, s.id, emp.id);
        }
    }
    
    public void plunderShipTech(final Tech t, final int empId) {
        final boolean newTech = this.tech().learnTech(t.id);
        if (newTech && this.isPlayer()) {
            this.log("Ship tech: ", t.name(), " plundered ");
            PlunderShipTechNotification.create(t.id, empId);
        }
    }
    
    public void plunderAncientTech(final StarSystem s) {
        s.planet().plunderBonusTech();
        final Tech t = this.tech().randomUnknownTech(10);
        final boolean newTech = this.tech().learnTech(t.id);
        if (newTech && this.isPlayer()) {
            this.log("Tech: ", t.name(), " discovered on: ", s.name());
            PlunderTechNotification.create(t.id, s.id, -1);
        }
    }
    
    public int maxRobotControls() {
        return this.tech.baseRobotControls() + this.race().robotControlsAdj();
    }
    
    public int baseRobotControls() {
        return 2 + this.race().robotControlsAdj();
    }
    
    public float workerProductivity() {
        final float bookFormula = (this.tech.planetology().techLevel() * 3.0f + 50.0f) / 100.0f;
        return bookFormula * this.race().workerProductivityMod();
    }
    
    public float totalIncome() {
        return this.netTradeIncome() + this.totalPlanetaryIncome();
    }
    
    public float netIncome() {
        return this.totalIncome() - this.totalShipMaintenanceCost();
    }
    
    public float empireTaxRevenue() {
        return this.totalTaxablePlanetaryProduction() * this.empireTaxPct() / 2.0f;
    }
    
    public float empireInternalSecurityCost() {
        return this.totalTaxablePlanetaryProduction() * this.internalSecurityCostPct();
    }
    
    public float empireExternalSpyingCost() {
        return this.totalTaxablePlanetaryProduction() * this.totalSpyCostPct();
    }
    
    public boolean incrementEmpireTaxLevel() {
        return this.empireTaxLevel(this.empireTaxLevel + 1);
    }
    
    public boolean decrementEmpireTaxLevel() {
        return this.empireTaxLevel(this.empireTaxLevel - 1);
    }
    
    public float empireTaxPct() {
        return this.empireTaxLevel / 100.0f;
    }
    
    public float maxEmpireTaxPct() {
        return this.maxEmpireTaxLevel() / 100.0f;
    }
    
    public int empireTaxLevel() {
        return this.empireTaxLevel;
    }
    
    public int maxEmpireTaxLevel() {
        return 20;
    }
    
    public boolean empireTaxLevel(final int i) {
        final int prevLevel = this.empireTaxLevel;
        this.empireTaxLevel = this.bounds(0, i, this.maxEmpireTaxLevel());
        return this.empireTaxLevel != prevLevel;
    }
    
    public boolean hasTrade() {
        for (final EmpireView v : this.empireViews()) {
            if (v != null && v.trade().level() > 0) {
                return true;
            }
        }
        return false;
    }
    
    public float netTradeIncome() {
        final float trade = this.totalTradeIncome();
        return (trade <= 0.0f) ? trade : (trade * (1.0f - this.tradePiracyRate));
    }
    
    public float totalTradeIncome() {
        float sum = 0.0f;
        for (final EmpireView v : this.empireViews()) {
            if (v != null) {
                sum += v.trade().profit();
            }
        }
        return sum;
    }
    
    public int totalTradeTreaties() {
        int sum = 0;
        for (final EmpireView v : this.empireViews()) {
            if (v != null) {
                sum += v.trade().level();
            }
        }
        return sum;
    }
    
    public float totalFleetSize(final Empire emp) {
        if (this == emp) {
            return this.totalFleetSize();
        }
        float spyPts = 0.0f;
        final SpyNetwork.FleetView fv = this.viewForEmpire(emp).spies().fleetView();
        if (!fv.noReport()) {
            spyPts += fv.small() * ShipDesign.hullPoints(0);
            spyPts += fv.medium() * ShipDesign.hullPoints(1);
            spyPts += fv.large() * ShipDesign.hullPoints(2);
            spyPts += fv.huge() * ShipDesign.hullPoints(3);
        }
        float visiblePts = 0.0f;
        for (final Ship sh : this.visibleShips()) {
            if (sh.empId() == emp.id && sh instanceof ShipFleet) {
                final ShipFleet sh2 = (ShipFleet)sh;
                visiblePts += sh2.hullPoints();
            }
        }
        return this.max(spyPts, visiblePts);
    }
    
    public Float totalFleetSize() {
        float pts = 0.0f;
        for (final ShipFleet fl : this.fleets()) {
            pts += fl.hullPoints();
        }
        return pts;
    }
    
    public Float totalArmedFleetSize() {
        float pts = 0.0f;
        for (int i = 0; i < 6; ++i) {
            final ShipDesign d = this.shipLab().design(i);
            if (d.active() && d.isArmed() && !d.isColonyShip()) {
                pts += d.buildCount() * d.hullPoints();
            }
        }
        return pts;
    }
    
    public float totalFleetCost() {
        float pts = 0.0f;
        for (final ShipFleet fl : this.fleets()) {
            pts += fl.bcValue();
        }
        return pts;
    }
    
    public Float totalPlanetaryPopulation() {
        float totalPop = 0.0f;
        for (int n = 0; n < this.sv.count(); ++n) {
            if (this.sv.empire(n) == this) {
                totalPop += this.sv.colony(n).population();
            }
        }
        return totalPop;
    }
    
    public float totalPlanetaryPopulation(final Empire emp) {
        float totalPop = 0.0f;
        if (emp == this) {
            final List<StarSystem> systems = new ArrayList<StarSystem>(this.allColonizedSystems());
            for (final StarSystem sys : systems) {
                totalPop += sys.colony().population();
            }
        }
        else {
            for (int n = 0; n < this.sv.count(); ++n) {
                if (this.sv.empire(n) == emp) {
                    totalPop += this.sv.population(n);
                }
            }
        }
        return totalPop;
    }
    
    public float totalPlanetaryIncome() {
        float totalProductionBC = 0.0f;
        final List<StarSystem> systems = new ArrayList<StarSystem>(this.allColonizedSystems());
        for (final StarSystem sys : systems) {
            totalProductionBC += sys.colony().totalIncome();
        }
        return totalProductionBC;
    }
    
    public float totalTaxablePlanetaryProduction() {
        float totalProductionBC = 0.0f;
        final List<StarSystem> systems = new ArrayList<StarSystem>(this.allColonizedSystems());
        for (final StarSystem sys : systems) {
            final Colony col = sys.colony();
            if (!col.embargoed()) {
                totalProductionBC += col.production();
            }
        }
        return totalProductionBC;
    }
    
    public Float totalPlanetaryProduction() {
        float totalProductionBC = 0.0f;
        final List<StarSystem> systems = new ArrayList<StarSystem>(this.allColonizedSystems());
        for (final StarSystem sys : systems) {
            totalProductionBC += sys.colony().production();
        }
        return totalProductionBC;
    }
    
    public float totalPlanetaryProduction(final Empire emp) {
        if (emp == this) {
            return this.totalPlanetaryProduction();
        }
        float totalProductionBC = 0.0f;
        for (int i = 0; i < this.sv.count(); ++i) {
            if (this.sv.empire(i) == emp && this.sv.colony(i) != null) {
                totalProductionBC += this.sv.colony(i).production();
            }
        }
        return totalProductionBC;
    }
    
    public float totalMissileBaseCostPct() {
        final float empireBC = this.totalPlanetaryProduction();
        float totalCostBC = 0.0f;
        final List<StarSystem> allSystems = new ArrayList<StarSystem>(this.allColonizedSystems());
        for (final StarSystem sys : allSystems) {
            totalCostBC += sys.colony().defense().missileBaseMaintenanceCost();
        }
        return totalCostBC / empireBC;
    }
    
    public float totalPlanetaryIndustrialSpending() {
        float totalIndustrialSpendingBC = 0.0f;
        final List<StarSystem> systems = new ArrayList<StarSystem>(this.allColonizedSystems());
        for (final StarSystem sys : systems) {
            totalIndustrialSpendingBC += sys.colony().pct(2) * sys.colony().totalIncome();
        }
        return totalIndustrialSpendingBC;
    }
    
    public float totalPlanetaryResearch() {
        float totalResearchBC = 0.0f;
        final List<StarSystem> systems = new ArrayList<StarSystem>(this.allColonizedSystems());
        for (final StarSystem sys : systems) {
            totalResearchBC += sys.colony().research().totalBCForEmpire();
        }
        return totalResearchBC;
    }
    
    public float totalEmpireResearch(final float totalRp) {
        float total = 0.0f;
        final TechTree t = this.tech();
        total += t.computer().currentResearch(totalRp);
        total += t.construction().currentResearch(totalRp);
        total += t.forceField().currentResearch(totalRp);
        total += t.planetology().currentResearch(totalRp);
        total += t.propulsion().currentResearch(totalRp);
        total += t.weapon().currentResearch(totalRp);
        return total;
    }
    
    public float totalPlanetaryResearchSpending() {
        float totalResearchBC = 0.0f;
        final List<StarSystem> systems = new ArrayList<StarSystem>(this.allColonizedSystems());
        for (final StarSystem sys : systems) {
            totalResearchBC += sys.colony().research().totalSpending();
        }
        return totalResearchBC;
    }
    
    public void allocateReserve(final Colony col, final int amount) {
        final float amt = this.min(this.totalReserve, (float)amount);
        this.totalReserve -= amt;
        col.adjustReserveIncome(amt);
    }
    
    public void goExtinct() {
        if (this.extinct) {
            return;
        }
        GenocideIncident.create(this, this.lastAttacker);
        GNNGenocideNotice.create(this, this.lastAttacker);
        this.extinct = true;
        this.galaxy().council().removeEmpire(this);
        final List<ShipFleet> fleetsToDisband = new ArrayList<ShipFleet>(this.fleets());
        for (final ShipFleet fl : fleetsToDisband) {
            this.log("disband#1 fleet: ", fl.toString());
            fl.disband();
        }
        if (this.galaxy().numActiveEmpires() == 1) {
            if (this.isPlayer()) {
                this.session().status().loseMilitary();
            }
            else {
                this.session().status().winMilitary();
            }
        }
        for (final EmpireView v : this.empireViews()) {
            if (v != null) {
                v.embassy().removeContact();
            }
        }
        this.status.assessTurn();
    }
    
    public ShipView shipViewFor(final ShipDesign d) {
        if (d == null) {
            return null;
        }
        if (d.empire() == this) {
            return this.shipLab.shipViewFor(d);
        }
        final EmpireView cv = this.viewForEmpire(d.empire());
        if (cv != null) {
            return cv.spies().shipViewFor(d);
        }
        return null;
    }
    
    private void detectFleet(final ShipFleet fl) {
        final EmpireView cv = this.viewForEmpire(fl.empire());
        if (cv == null) {
            return;
        }
        final int[] visible = fl.visibleShips(this);
        for (int i = 0; i < visible.length; ++i) {
            if (visible[i] > 0) {
                cv.spies().detectShip(fl.empire().shipLab().design(i));
            }
        }
    }
    
    public void encounterFleet(final ShipFleet fl) {
        final EmpireView cv = this.viewForEmpire(fl.empire());
        if (cv == null) {
            return;
        }
        final int[] visible = fl.visibleShips(this);
        for (int i = 0; i < visible.length; ++i) {
            if (visible[i] > 0) {
                cv.spies().encounterShip(fl.empire().shipLab().design(i));
            }
        }
    }
    
    public void scanFleet(final ShipFleet fl) {
        final EmpireView cv = this.viewForEmpire(fl.empire());
        if (cv == null) {
            return;
        }
        final int[] visible = fl.visibleShips(this);
        for (int i = 0; i < visible.length; ++i) {
            if (visible[i] > 0) {
                cv.spies().scanShip(fl.empire().shipLab().design(i));
            }
        }
    }
    
    public void scanDesign(final ShipDesign st, final Empire emp) {
        final EmpireView cv = this.viewForEmpire(emp);
        if (cv != null) {
            cv.spies().scanShip(st);
        }
    }
    
    public List<StarSystem> orderedColonies() {
        final List<StarSystem> list = new ArrayList<StarSystem>(this.allColonizedSystems());
        Collections.sort(list, IMappedObject.MAP_ORDER);
        return list;
    }
    
    public List<StarSystem> orderedTransportTargetSystems() {
        final Galaxy gal = this.galaxy();
        final List<StarSystem> list = new ArrayList<StarSystem>();
        for (int i = 0; i < this.sv.count(); ++i) {
            final StarSystem sys = gal.system(i);
            if (this.sv.inShipRange(i) && this.sv.isScouted(i) && this.sv.isColonized(i) && this.tech().canColonize(sys.planet())) {
                list.add(sys);
            }
        }
        Collections.sort(list, IMappedObject.MAP_ORDER);
        return list;
    }
    
    public List<StarSystem> orderedFleetTargetSystems(final ShipFleet fl) {
        final float range = fl.range();
        final Galaxy gal = this.galaxy();
        final List<StarSystem> list = new ArrayList<StarSystem>();
        for (int n = 0; n < this.sv.count(); ++n) {
            if (this.sv.withinRange(n, range)) {
                list.add(gal.system(n));
            }
        }
        Collections.sort(list, IMappedObject.MAP_ORDER);
        return list;
    }
    
    public List<StarSystem> orderedShipConstructingColonies() {
        final List<StarSystem> list = new ArrayList<StarSystem>(this.shipBuildingSystems);
        Collections.sort(list, IMappedObject.MAP_ORDER);
        return list;
    }
    
    public List<ShipFleet> orderedFleets() {
        final List<ShipFleet> list = new ArrayList<ShipFleet>();
        for (final ShipFleet fl : this.fleets()) {
            if (!fl.isEmpty()) {
                list.add(fl);
            }
        }
        Collections.sort(list, IMappedObject.MAP_ORDER);
        return list;
    }
    
    public List<ShipFleet> orderedEnemyFleets() {
        final List<ShipFleet> list = new ArrayList<ShipFleet>(this.enemyFleets());
        Collections.sort(list, IMappedObject.MAP_ORDER);
        return list;
    }
    
    public List<StarSystem> orderedUnderAttackSystems() {
        final List<StarSystem> list = new ArrayList<StarSystem>();
        if (this.knowShipETA) {
            for (final ShipFleet fl : this.enemyFleets()) {
                if (fl.inTransit()) {
                    final StarSystem sys = fl.destination();
                    if (this.sv.empire(sys.id) != this || list.contains(sys)) {
                        continue;
                    }
                    list.add(sys);
                }
                else {
                    final StarSystem sys = fl.system();
                    if (this.sv.empire(sys.id) != this || list.contains(sys)) {
                        continue;
                    }
                    list.add(sys);
                }
            }
        }
        return list;
    }
    
    public static Set<Empire> allContacts(final Empire e1, final Empire e2) {
        final Set<Empire> contacts = new HashSet<Empire>();
        contacts.addAll(e1.contactedEmpires());
        contacts.addAll(e2.contactedEmpires());
        contacts.remove(e1);
        contacts.remove(e2);
        return contacts;
    }
    
    static {
        Empire.TOTAL_POPULATION = ((o1, o2) -> o2.totalPlanetaryPopulation().compareTo(o1.totalPlanetaryPopulation()));
        Empire.TOTAL_PRODUCTION = ((o1, o2) -> o2.totalPlanetaryProduction().compareTo(o1.totalPlanetaryProduction()));
        Empire.AVG_TECH_LEVEL = ((o1, o2) -> o2.tech.avgTechLevel().compareTo(o1.tech.avgTechLevel()));
        Empire.TOTAL_FLEET_SIZE = ((o1, o2) -> o2.totalFleetSize().compareTo(o1.totalFleetSize()));
    }
}
