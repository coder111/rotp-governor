rm -rf target
mkdir target
javac -d target -cp lib/Remnants.jar -source 8 -target 8 \
    src/rotp/model/colony/Colony.java \
    src/rotp/model/empires/*.java \
    src/rotp/ui/main/EmpireColonySpendingPane.java \
    src/rotp/ui/main/EmpireSystemPanel.java \
    src/rotp/ui/planets/PlanetsUI.java

( cd target; jar cf rotp-governor.jar *)

