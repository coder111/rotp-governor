# rotp-governor

Planetary Governor mod for Remnants of the Precursors

https://remnantsoftheprecursors.com/

https://rayfowler.itch.io/remnants-of-the-precursors

This governor manages planet spending to:

* Set ecology to minimum "clean"
* Set max production until all factories are built.
* Set max ecology until max population is reached.
* Set max defence until required number of bases is built.
* Build a stargate if technology is available.
* If all above have been built, research.

It can be toggled on or off for each planet. You can basically enable it on any
planet not building ships and leave it untouched for most of the game. With new 
tech discoveries it will readjust the sliders automatically. This cuts down the
amount of micromanagement needed drastically.

To run the mod:

* Download rotp-governor.jar and run.bat or run.sh if you are on Linux.
* Place them in same directory that contains Remnants.jar
* Execute run.bat or run.sh

To enable governor, use 'q' key on keyboard, or else click "Allocate Spending"
text in the planetary spending screen.

Current implementation is quite crude and not optimal as I do not have access
to ROTP source code. But it works. There's room for improvement which I plan to
do once ROTP source is available.

Roy is planning to release official source code for ROTP in April 2020. We can 
develop more advanced mod and include UI features into the mod then.

---

Additional features.

* This mod will transport population from planets that are full to planets that
are underpopulated. Population from planets with maximum population will be 
transported. Only population that will grow back in 1 turn will be transported 
(usually 1-2 pop). When chosing destination, target population and distance will
be taken into account. If you want to turn this off, add "-Dautotransport=false" 
to Java command line like this:

java -cp rotp-governor.jar:Remnants.jar -Dautotransport=false -Xmx2048m rotp.Rotp arg1

* This mod will build stargates on all planets when technology is available. If you
want to turn this off, add "-Dautogate=false" to Java command line.

java -cp rotp-governor.jar:Remnants.jar -Dautogate=false -Xmx2048m rotp.Rotp arg1

---

# Building from source

Source files are decompiled. Original files and modified files are included in
the project.  

To build from source:

* Place Remants.jar in the lib/ directory
* Use build.sh provided
* This should produce target/rotp-governor.jar
* Run it as per instructions above

